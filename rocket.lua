Rocket = {}

function Rocket:new(params)
  o = {}
  o.speed = params.speed
  o.dir = params.dir -- direction as radians, zero being up
  o.x = params.x
  o.y = params.y
  o.type = params.type
  o.xspeed = 0
  o.yspeed = 0
  o.acceleration = 1.01 -- multiplier, greater than 1 or it's not acceleration!
  o.maxspeed = 2500

  o.smokecounter = 0
  o.smokefreq = 1

  o.removeMe = false

  setmetatable(o, self)
  self.__index = self
  return o
end

function Rocket:draw(x, y, angle, size)

		sidepointleftX = x + math.cos(angle+(1.57079633+0.5))*20*size
		sidepointleftY = y + math.sin(angle+(1.57079633+0.5))*20*size
		sidepointrightX = x + math.cos(angle-(1.57079633+0.5))*20*size
		sidepointrightY = y + math.sin(angle-(1.57079633+0.5))*20*size

    love.graphics.setColor(255, 255, 255)
    love.graphics.setLineWidth(1 + size)

		-- lines from back to sidepoints
		love.graphics.line(((x-(math.cos(angle))*30*size)),((y-(math.sin(angle))*30*size)) ,sidepointleftX,sidepointleftY)
		love.graphics.line(((x-(math.cos(angle))*30*size)),((y-(math.sin(angle))*30*size)) ,sidepointrightX,sidepointrightY)

		-- lines from sidepoints to fwdpoint
		love.graphics.line(sidepointleftX, sidepointleftY,((x+(math.cos(angle))*640*size)),((y+(math.sin(angle))*640*size)))
		love.graphics.line(sidepointrightX, sidepointrightY,((x+(math.cos(angle))*640*size)),((y+(math.sin(angle))*640*size)))
end

function Rocket:update(dt)
    self.speed = self.speed * self.acceleration
    if self.speed > self.maxspeed then self.speed = self.maxspeed end
    self.xspeed = math.cos(self.dir) * self.speed
    self.yspeed = math.sin(self.dir) * self.speed
    self.x = self.x + self.xspeed * dt
    self.y = self.y + self.yspeed * dt
    if (self.x < -10) or (self.x > universe.width + 10)
    or (self.y < -10) or (self.y > universe.height + 10) then
      self.removeMe = true
    end
    self.smokecounter = self.smokecounter + 1
    if self.smokecounter > self.smokefreq then
      self.smokecounter = 0
      table.insert(smokes, Smoke:new({
        dir = self.dir,
        size = 15,
        x = self.x,
        y = self.y}))
    end
end
