Turret = {}

function Turret:new(params)
  o = {}
  o.angle = params.angle -- cannon direction as radians (2*pi is 360 degrees), zero is towards right
  o.x = params.x
  o.y = params.y
  o.type = params.type
  o.xspeed = 0
  o.yspeed = 0
  o.heat = math.random(20)/100 -- start shooting at different moments (unison is not cool)
	o.heatp = 0.4 -- ST
  o.range = 100000 -- 100000 is infinite in game universe
  if o.type == "RFT" then -- rapid fire
    o.heatp = 0.04
    o.range = 4500
  elseif o.type == "RGT" then
    o.heatp = 0.08
    o.range = 2500 -- railguns have limited range so you can't just place them on the bottom
  elseif o.type == "RLT" then
    o.heatp = 1.5 -- rocket launcher is slow
  end

  setmetatable(o, self)
  self.__index = self
  return o
end

function Turret:update(dt)
  self.x = self.x + self.xspeed
  self.y = self.y + self.yspeed

  -- universe boundaries
  if self.x < 40 then self.xspeed = 1  end
  if self.x > universe.width-40 then self.xspeed = -1  end
  if self.y < 40  then self.yspeed = 1 end
  if self.y > universe.height-40 then self.yspeed = -1 end

  -- motion slowing down
  self.xspeed = self.xspeed * (1-SLOWING*2*dt)
  self.yspeed = self.yspeed * (1-SLOWING*2*dt)

  self:aim(dt)
end

function Turret:aim(dt)
  local s = gameStates.maingame
  -- aim turret at global target coordinates (if not zero)
  self.heat = math.max(0, self.heat - dt) -- cannon heat decreases - can shoot when heat zero

  -- find closest target that is in range -- for types 1 and 2 target enemy closest to base
  local targetIndex = self:targetNearestEnemy()
  if targetIndex > 0 then  -- if target acquired then for some action

    -- types 1 and 2 shoot bullets at self.angle
    if self.type == "ST" or self.type == "RFT" then
      -- aim exactly at target (this concentrates fire close to the swarm point, but not exactly)
      self.angle = - math.atan2(enemies[targetIndex].x - self.x, enemies[targetIndex].y - self.y) + math.pi/2
      if self.type == "ST" then self.angle = self:findAdvance(targetIndex) end -- except type one sniper takes advance into account

      local maxrange = 100000
      local bulletspeed = 10000
      local bullettype = 1
      if self.type == "RFT" then
        self.angle = self.angle + (((math.random(100)/50)-1))/8
        bulletspeed = 4000
        bullettype = 2
        maxrange = 4500
      end

      if self.heat <= 0 then
        local direction = self.angle
        table.insert(bullets, Bullet:new({
          x = self.x+(math.cos(self.angle)*80),
          y = self.y+(math.sin(self.angle)*80),
          maxrange = maxrange,
          dir = direction,
          type = bullettype,
          speed = bulletspeed}))
        self.heat = self.heatp

        if not(isAudioEffectsOff) then
          audio.turRot = audio.turRot + 1 -- rotating sound source for turrets
          if audio.turRot == 16 then audio.turRot = 13 end
          audio.SoundFx[audio.turRot]:stop()
          audio.SoundFx[audio.turRot]:setVolume(0.1)
          audio.SoundFx[audio.turRot]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
          audio.SoundFx[audio.turRot]:play()
        end
      end
    end

    if self.heat <= 0 and self.type == "RGT" then -- fire at will (as soon as cannon heat allows)
      -- aim exactly at target
      self.angle = - math.atan2(enemies[targetIndex].x - self.x, enemies[targetIndex].y - self.y) + math.pi/2
      if createRailGunBlast(self.x+(math.cos(self.angle)*30), self.y+(math.sin(self.angle)*30), enemies[targetIndex].x, enemies[targetIndex].y) then
        enemies[targetIndex].alive = false
        enemies[targetIndex].isMark = true
      end
      if not(isAudioEffectsOff) then
        audio.railRot = audio.railRot + 1 -- rotating sound source for turrets
        if audio.railRot == 27 then audio.railRot = 24 end
        audio.SoundFx[audio.railRot]:stop()
        audio.SoundFx[audio.railRot]:setVolume(0.05)
        audio.SoundFx[audio.railRot]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
        audio.SoundFx[audio.railRot]:play()
      end
      self.heat = self.heatp
    end

    if self.heat <= 0 and self.type == "RLT" then -- fire at will (as soon as cannon heat allows)
      -- aim exactly at target
      self.angle = - math.atan2(enemies[targetIndex].x - self.x, enemies[targetIndex].y - self.y) + math.pi/2
      local direction = self.angle
      table.insert(rockets, Rocket:new({
        x = self.x+(math.cos(self.angle)*46),
        y = self.y+(math.sin(self.angle)*46),
        dir = direction,
        type = "small",
        speed = 1000}))
      self.heat = self.heatp
      if not(isAudioEffectsOff) then
        audio.SoundFx[27]:stop()
        audio.SoundFx[27]:setVolume(0.1)
        audio.SoundFx[27]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
        audio.SoundFx[27]:play()
      end
    end

  else -- rotate slowly if no target acquired
    self.angle = self.angle + 0.01
    if self.angle > math.pi*2 then self.angle = self.angle - math.pi*2 end
  end
end

function Turret:draw(x, y, angle)

  if self.type == "ST" then
    love.graphics.setColor(255, 255, 0)
    love.graphics.draw(turret1sprite, x, y, angle+math.pi/2, 1, 1, 89, 124)

--[[
    -- red dot at aim point for sniper turrets
    love.graphics.setColor(255, 0, 0)
    love.graphics.setPointSize(10)
    love.graphics.points(targetX, targetY)
    ]]--

  elseif self.type == "RFT" then
    love.graphics.setColor(100, 100, 255) -- colour fill
    love.graphics.draw(turret4sprite, x, y, angle+math.pi/2, 1, 1, 89, 124)

  elseif self.type == "RGT" then
    love.graphics.setColor(255, 0, 200) -- colour fill
    love.graphics.draw(turret3sprite, x, y, angle+math.pi/2, 1, 1, 89, 124)

  elseif self.type == "RLT" then
    love.graphics.setColor(0, 255, 255) -- colour fill
    love.graphics.draw(turret2sprite, x, y, angle+math.pi/2, 1, 1, 89, 124)

  end


end


-- returns the index of closest enemy that is within range, if none, then return 0
function Turret:targetNearestEnemy()
  local s = gameStates.maingame
  local i
  local minDistance = 100000
  local candidateIndex = 0
  local distance = 0
  local distance2 = 0

  for i, enemy in ipairs(enemies) do

    -- distance to base
    distance = math.sqrt((enemy.x - (universe.width/2))*(enemy.x - (universe.width/2)) + (enemy.y - universe.height)*(enemy.y - universe.height))
    -- distance to turret
    distance2 = math.sqrt((enemy.x - self.x)*(enemy.x - self.x) + (enemy.y - self.y)*(enemy.y - self.y))

    if self.type == "ST" or self.type == "RLT" or self.type == "RFT" then
      -- always select enemy, which is in range and closest to base
      if (distance < minDistance) and (enemy.isMark == false) and distance2 < self.range then
        minDistance = distance
        candidateIndex = i
      end

    elseif self.type == "RGT" then
      -- always select enemy closest to turret
      if (distance2 < minDistance) and (enemy.isMark == false) and distance2 < self.range then
        minDistance = distance2
        candidateIndex = i
      end
    end

  end
  return candidateIndex
end

function Turret:findAdvance(enemyId)
  local s = gameStates.maingame
  local distance = math.sqrt((self.x-enemies[enemyId].x)*(self.x-enemies[enemyId].x)+(self.y-enemies[enemyId].y)*(self.y-enemies[enemyId].y))
  -- for type 1 bullet we know the speed is 10000
  local adjustment = 0.9 -- this was found by testing when the fire really hits the enemy
  local timeToTarget = distance/10000 * adjustment
  targetX = enemies[enemyId].x + enemies[enemyId].xspeed * timeToTarget
  targetY = enemies[enemyId].y + enemies[enemyId].yspeed * timeToTarget


  -- aim at the point where the enemy will advance in the same time the bullet would reach it
  return - math.atan2(targetX - self.x, targetY - self.y) + math.pi/2
end
