Inventory = {}

require 'item'

function Inventory:new(params)
  o = {}
  o.Items = {}
  o.Saldos = {}
  o.mainWeapon = "RFCI"

  setmetatable(o, self)
  self.__index = self
  return o
end

function Inventory:update(dt)
end

-- returns the id (string) of the next available weapon, given the id of the current weapon
-- called from weaponConsole
function Inventory:getNextWeapon(currentWeapon, isStartGame)
  local i
  -- find index of the first inventory item that has saldo and is allowed in this gamestate

  for i=currentWeapon+1,#self.Saldos do
    if self.Saldos[i]>0 then
      if not isStartGame then return self.Items[i].id end
      if isStartGame and self.Items[i].allowedInStartgame then return self.Items[i].id end
    end
  end
  for i=1,currentWeapon do
    if self.Saldos[i]>0 then
      if not isStartGame then return self.Items[i].id end
      if isStartGame and self.Items[i].allowedInStartgame then return self.Items[i].id end
    end
  end
  return "" -- all item saldos were zero!
end


function Inventory:initiateInventory()
  inventory.Items = {}
  inventory.Saldos = {}

  local item = Item:new({
    name = "RFC Rapid-Fire Cannon I",
    id = "RFCI",
    descr = "The default cannon.",
    category = "Main weapon upgrades",
    icon = gun1icon,
    price = 0,
    isTurret = false,
    allowedInStartgame = false})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 1)

  local item = Item:new({
    name = "RFC Rapid-Fire Cannon II",
    id = "RFCII",
    descr = "Two pairs of cannon Double the destruction!",
    category = "Main weapon upgrades",
    icon = gun1icon,
    price = 100000,
    isTurret = false,
    allowedInStartgame = false})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "RFC Rapid-Fire Cannon III",
    id = "RFCIII",
    descr = "Multi-mounted cannons for extra mayhem.",
    category = "Main weapon upgrades",
    icon = gun1icon,
    price = 200000,
    isTurret = false,
    allowedInStartgame = false})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "RFC Rapid-Fire Cannon IV",
    id = "RFCIV",
    descr = "Twin cannons with spreadfire together with insane mine projectile launchers of doom.",
    category = "Main weapon upgrades",
    icon = gun1icon,
    price = 5, -- 50000,
    isTurret = false,
    allowedInStartgame = false})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "DIM Direct Impact Mine",
    id = "DIM",
    descr = "Explodes on impact with enemy drone Massive explosion",
    category = "One-time weapons",
    icon = mine1icon,
    price = 5000,
    isTurret = false,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "DIR Direct Impact Rocket",
    id = "DIR",
    descr = "Nice and powerful. Accelerates in one direction until hits a target and explode",
    category = "One-time weapons",
    icon = msle1icon,
    price = 3000,
    isTurret = false,
    allowedInStartgame = false})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "ST Sniper Turret",
    id = "ST",
    descr = "Long range accurate shots, but low rate of fire. Cheap!",
    category = "Turrets",
    icon = turret1icon,
    price = 7500,
    isTurret = true,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 7)

  local item = Item:new({
    name = "RFT Rapid-Fire Turret",
    id = "RFT",
    descr = "Insane rate of fire, but low accuracy and limited range. Aims at enemies closest to base.",
    category = "Turrets",
    icon = turret2icon,
    price = 20000,
    isTurret = true,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "RGT Rail-Gun Turret",
    id = "RGT",
    descr = "Short range, 100% certain destruction. High rate of fire. Aims at enemies closest to turret.",
    category = "Turrets",
    icon = turret3icon,
    price = 25000,
    isTurret = true,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "RLT Rocket Launcher Turret",
    id = "RLT",
    descr = "Shoots explosive rockets at enemies closest to base. A hurricane of fiery destruction!",
    category = "Turrets",
    icon = turret4icon,
    price = 75000,
    isTurret = true,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

  local item = Item:new({
    name = "LW Light Wall",
    id = "LW",
    descr = "Fast to move around. Endures quite a few hits before destroyed.",
    category = "Blockades",
    icon = wall1icon,
    price = 10000,
    isTurret = false,
    allowedInStartgame = true})

  table.insert(self.Items, item)
  table.insert(self.Saldos, 0)

end

-- weaponConsole identifies selected weapon by id (string), this function translates is to the index of the inventory.Items list
function Inventory:returnIndex(w_id)
  local i
  for i=1,#self.Items do
    if self.Items[i].id == w_id then
      return i
    end
  end
  return 0
end

function Inventory:returnNextWeapon()
  if self.mainWeapon == "RFCI" then return "RFCII"
  elseif self.mainWeapon == "RFCII" then return "RFCIII"
  elseif self.mainWeapon == "RFCIII" then return "RFCIV"
  elseif self.mainWeapon == "RFCIV" then return "none" end
end

function Inventory:returnFirstTurret()
  local i = 1
  repeat
    if self.Items[i].category == "Turrets" then return i end
    i = i + 1
  until i > #self.Items
  return 0
end
