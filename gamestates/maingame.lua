gameStates.maingame = {}
s = gameStates.maingame -- short ref to maingame state
s.isInitiated = false

require 'ship'
require 'bullet'
require 'enemy'
require 'turret'
require 'asteroid'
require 'textlogger'
require 'weaponconsole'
require 'railgunblasts'
require 'swarmpath'
require 'surrrge'
require 'explosion'
require 'mine'
require 'rocket'
require 'smoke'
require 'collisions'
require 'flashtext'
require 'itempack'
require 'enemyspawner'
require 'gameevent'
require 'gamestates/levels'

function gameStates.maingame.initiateState()
  s.highscore = 0
  s.resetGame()
end

function gameStates.maingame.draw()
  -- first draw zoomable game graphics
  love.graphics.push()
  love.graphics.setColor(255, 255, 255)
  love.graphics.setLineWidth(1)
  love.graphics.scale(tv("scale"), tv("scale"))
  love.graphics.translate(scrolloffsetX, scrolloffsetY)

  -- draw background image which is as large as the game universe
  love.graphics.draw(bg, 0, 0, 0, UNIVERSESIZE, UNIVERSESIZE)

  -- draw parallax scrolling second layer backround
--    love.graphics.draw(bg2, scrolloffsetX/10, scrolloffsetY/10, 0, UNIVERSESIZE*2, UNIVERSESIZE*2)

  drawBullets()
  drawEnemies()
  drawTurrets()
  drawAsteroids()
  drawRailGunBlasts()
  drawMines()
  drawSmokes()
  drawRockets()
  drawExplosions()
  drawFlashtexts()
  ship:draw()
  swarmPathsHandler:draw()

  -- then reset transformations and draw static overlay graphics such as texts and menus
  love.graphics.pop()
  textLogger:draw()
  weaponConsole:draw()
  if radarOn then drawRadar() end
  drawTexts()
  drawItemPacks()
  love.graphics.print("Current FPS: " .. tostring(currentFPS), 10, 10)
end

function drawTexts()
  love.graphics.setColor(255, 255, 255, tv("alpha"))
  msg:clear()
  if isStartGame then
    local index = msg:add("DEPLOY - O           SWITCH - P", 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12
    love.graphics.draw(msg, text_x, tv("textY"))
    msg:clear()
    local index = msg:add("PRESS SPACE WHEN READY", 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12 * 8
    love.graphics.draw(msg, text_x, tv("textY"))
  else
    local index = msg:add("KILLS: " .. kills, 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12
    love.graphics.draw(msg, text_x, tv("textY"))
  end
  if isSpawningPaused then
    -- show time until next wave
    msg2:clear()
    local index = msg2:add("TIME UNTIL NEXT WAVE " .. timeUntilNextWave, 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12 * 3
    love.graphics.draw(msg2, text_x, 600)
  end
end

function drawMines()
		for i, o in ipairs(mines) do
			o:draw(o.x, o.y, o.angle)
		end
end

function drawItemPacks()
		for i, o in ipairs(itemPacks) do
			o:draw()
		end
end

function drawExplosions()
		for i, o in ipairs(explosions) do
			o:draw()
		end
end

function drawFlashtexts()
		for i, o in ipairs(flashTexts) do
			o:draw()
		end
end

function drawSmokes()
		for i, o in ipairs(smokes) do
			o:draw()
		end
end

function drawRailGunBlasts()
		for i, o in ipairs(railGunBlasts) do
			o:draw()
		end
end

function drawBullets()
		for i, o in ipairs(bullets) do
			o:draw()
		end
end

function drawRockets()
		for i, o in ipairs(rockets) do
			o:draw(o.x, o.y, o.dir, 0.2)
		end
end

function drawEnemies()
	drawnEnemyCoords = {} -- list all drawn coordinates and do not draw duplicates
	local counter = 0
	for i, o in ipairs(enemies) do
		if not(isCoordinateOnList(o.x, o.y)) then
			table.insert(drawnEnemyCoords, {o.x, o.y})
			o:draw(o.x, o.y, o.angle, 1)
		end
	end
end

function isCoordinateOnList(x, y)
	local testCoords = {x, y}
	local threshold = 10
	for i=1,#drawnEnemyCoords do
		if math.abs(drawnEnemyCoords[i][1] - testCoords[1]) < threshold and math.abs(drawnEnemyCoords[i][2] - testCoords[2]) < threshold then return true end
	end
	return false
end


function drawTurrets()
		for i, o in ipairs(turrets) do
			o:draw(o.x, o.y, o.angle, 1)
		end
end

function drawAsteroids()
		for i, o in ipairs(asteroids) do
			o:draw(o.x, o.y)
		end
end

function drawRadar() -- is a black box where enemies are plotted
	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle("fill", love.graphics.getWidth()-50, 20, 40, love.graphics.getHeight()-40)

	local radarX
	local radarY

	love.graphics.setColor(255, 0, 150)
	love.graphics.setPointSize(3)
	for i, o in ipairs(enemies) do
		radarX = o.x/universe.width*40 + love.graphics.getWidth()-50
		radarY = o.y/(universe.height) * (love.graphics.getHeight()-40) + 20
		if o.alive then love.graphics.points(radarX, radarY) end
	end
	love.graphics.setColor(255, 255, 255)
	love.graphics.setPointSize(5)
	for i, o in ipairs(turrets) do
		radarX = o.x/universe.width*40 + love.graphics.getWidth()-50
		radarY = o.y/(universe.height) * (love.graphics.getHeight()-40) + 20
		love.graphics.points(radarX, radarY)
	end
	love.graphics.setColor(0, 255, 0)
	radarX = ship.x/universe.width*40 + love.graphics.getWidth()-50
	radarY = ship.y/(universe.height) * (love.graphics.getHeight()-40) + 20
	love.graphics.points(radarX, radarY)
end

function drawOverlay() -- called from substates getready and gameover
  love.graphics.setColor(0, 0, 0, 150)
  love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
end

function gameStates.maingame.keypressed(key)
  if key == "space" then
    if isStartGame then isStartGame = false end
--    s.isPaused = not(s.isPaused) -- switch pause on and off
  elseif key == "z" then -- use for debugging calls
    drawPathsDebug = not(drawPathsDebug)
  elseif key == "p" and not weaponConsole.animNow then
    weaponConsole:switchWeapon(isStartGame)
  elseif key == "r" then -- toggle radar
    radarOn = not(radarOn)

  elseif key == "2" then
    local currentScale = tv("scale")
    if not(currentScale == love.graphics.getHeight() / universe.height) then
      tweenEngine:createTween("scale", currentScale, love.graphics.getHeight() / universe.height, 0.5, easeOutQuint)
    end
  elseif key == "1" then
    local currentScale = tv("scale")
    if not(currentScale == love.graphics.getWidth() / universe.width) then
      tweenEngine:createTween("scale", currentScale, love.graphics.getWidth() / universe.width, 0.5, easeOutQuint)
    end
  end
end

function gameStates.maingame.update(dt)
  if not isInitiated then
    isInitiated = true
    gameStates.maingame.initiateState()
  end

  -- after first 0.5 seconds in game, go to substate getready and display instructions & stuff
  if currentSubState == "none" and timeInState > 0.2 and timeInState < 1 then
    currentSubState = "getready"
    s.isPaused = true
  end

  if not s.isPaused then
    -- update
    if cameraOnShip then calculateOffsets(ship.x, ship.y) else calculateOffsets(universe.width/2, tv("cameraY")) end

		-- key controls
    if not s.isControlsDisabled then
      if love.keyboard.isDown("a") then
      	if love.keyboard.isDown("o") then
      	   ship.angle = ship.angle - (ship.turnspeed*dt/5)
  			else
          ship.angle = ship.angle - ship.turnspeed*dt
  			end
  			if ship.angle < 0 then ship.angle = ship.angle + (2*math.pi) end
      end

      if love.keyboard.isDown("d") then
      	if love.keyboard.isDown("o") then
      		ship.angle = ship.angle + (ship.turnspeed*dt/5)
  			else ship.angle = ship.angle + ship.turnspeed*dt
  			end
  			if ship.angle > (2*math.pi) then ship.angle = ship.angle - (2*math.pi) end
    	end

      if love.keyboard.isDown("w") then
  			ship.xspeed = ship.xspeed + math.cos(ship.angle)*dt*60
  			ship.yspeed = ship.yspeed + math.sin(ship.angle)*dt*60
      end

  		if love.keyboard.isDown("s") then
  			ship.xspeed = ship.xspeed - math.cos(ship.angle)*dt*60
  			ship.yspeed = ship.yspeed - math.sin(ship.angle)*dt*60
      end

  		if love.keyboard.isDown("o")  and ship.heat <= 0 then
  			weaponConsole:fireWeapon(ship.x, ship.y, ship.angle)
  		end
  		if love.keyboard.isDown("o")  and ship.mheat <= 0 then
  			weaponConsole:fireMineProjectiles(ship.x, ship.y, ship.angle)
  		end
    end

    ship.heat = math.max(0, ship.heat - dt) -- cannon heat decreases - can shoot when heat zero
    ship.mheat = math.max(0, ship.mheat - dt) -- mine projectile cannon heat decreases - can shoot when heat zero

		ship:update(dt)
		textLogger:update(dt)
    inventory:update(dt)
		weaponConsole:update(dt) -- when weaponConsole initiates, inventory must have already initiated!
		swarmPathsHandler:update(dt)

    if not(currentSubState == "gameover") then
      -- distribution of itemPacks, the limit of kills you need for the next grows exponentially
      if nextItemPackCounter >= nextItemPackLimit then
        nextItemPackCounter = 0
        nextItemPackLimit = nextItemPackLimit + nextItemPackLimitGrowth
        nextItemPackLimitGrowth = nextItemPackLimitGrowth + 25 -- growth exponentially
        generateItemPack()
      end

      -- weapon upgrade itempacks are pushed at certain amounts of kills
      if kills >= 5000 and weaponUpgradeIterator == 0 then
        weaponUpgradeIterator = weaponUpgradeIterator + 1
        generateWeaponUpgrade()
      elseif kills >= 20000 and weaponUpgradeIterator == 1 then
        weaponUpgradeIterator = weaponUpgradeIterator + 1
        generateWeaponUpgrade()
      elseif kills >= 50000 and weaponUpgradeIterator == 2 then
        weaponUpgradeIterator = weaponUpgradeIterator + 1
        generateWeaponUpgrade()
      end
    end

    -- update enemySpawners:
    if not isSpawningPaused then
    	local i2, o
    	for i2, o in ipairs(enemySpawners) do
        o:update(dt)
    	end
    else
      local timePaused = love.timer.getTime() - spawningPauseStamp
      timeUntilNextWave = 30 - math.floor(timePaused)
      if timePaused > 30 then isSpawningPaused = false end
    end

    if not(currentSubState == "gameover") then enemySpawnerIterator = enemySpawnerIterator + 1 end
    if enemySpawnerIterator == enemySpawnerAccelerationInterval then
      increaseSpawningVelocity()

      enemySpawnerIterator = 0
    end

    -- update itemPacks:
--  	local i2, o
  	for i2, o in ipairs(itemPacks) do
  		o:update(dt)
  		if o.removeMe then table.remove(itemPacks, i2) end
  	end

    -- update flashTexts:
--  	local i2, o
  	for i2, o in ipairs(flashTexts) do
  		o:update(dt)
  		if o.removeMe then table.remove(flashTexts, i2) end
  	end

		-- update bullets:
--		local i2, o
		for i2, o in ipairs(bullets) do
			o:update(dt)
			if o.removeMe then table.remove(bullets, i2) end
		end

		-- update rockets:
--		local i2, o
		for i2, o in ipairs(rockets) do
			o:update(dt)
			if o.removeMe then table.remove(rockets, i2) end
		end

		-- update mines:
--		local i2, o
		for i2, o in ipairs(mines) do
			o:update(dt)
			if o.removeMe then table.remove(mines, i2) end
		end

		-- update explosions:
--		local i2, o
		for i2, o in ipairs(explosions) do
			o:update(dt)
			if o.removeMe then table.remove(explosions, i2) end
		end


		-- update smokes = smoke puffs:
--		local i2, o
		for i2, o in ipairs(smokes) do
			o:update(dt)
			if o.removeMe then table.remove(smokes, i2) end
		end

		-- update enemy surges:
--		local i2, o
		for i2, o in ipairs(surrrges) do
			o:update(dt)
			if o.removeMe then table.remove(surrrges, i2) end
		end

		-- update railgun blasts:
--		local i2, o
		for i2, o in ipairs(railGunBlasts) do
			o:update(dt)
			if railGunBlasts[i2].removeMe then table.remove(railGunBlasts, i2) end
		end

		-- update enemies:
--		local i2, o
		for i2, o in ipairs(enemies) do
			o:update(dt)
			if enemies[i2].explosionState >= 13 then removeEnemy(i2) end
		end

		-- update turrets:
--		local i2, o
		for i2, o in ipairs(turrets) do
			o:update(dt)
		end

		-- update asteroids:
--		local i2, o
		for i2, o in ipairs(asteroids) do
			o:update(dt)
		end

    checkCollisions(s) -- push to separate file collisionlua

  end
end

function increaseSpawningVelocity()
  -- find first spawner that has velocity less than 100
  local i, i2, o
  local increaserIndex = 0
  for i, o in ipairs(enemySpawners) do
    if o.velocity < 100 then
      increaserIndex = i
      break
    end
  end
  if increaserIndex > 0 then
    enemySpawners[increaserIndex]:increaseVelocity()
  else -- none of the spawners had velocity less than 100 --> create a new spawner
    -- find first path that has less than 3 spawners
    for i2=1, #spawnersOnPath do
      if spawnersOnPath[i2] < 3 then
        spawnersOnPath[i2] = spawnersOnPath[i2] + 1
        table.insert(enemySpawners, Enemyspawner:new({
          pathId = i2}))
        break
      end
      -- none found, spawners are at max capacity, start increasing the yspeed of enemies
      ySpeedIncrease = ySpeedIncrease + 1
    end
  end
end

function removeEnemy(i)
	kills = kills + 1
  nextItemPackCounter = nextItemPackCounter + 1
	credits = credits + 5
	table.remove(enemies, i)

  --[[
	-- explosion soundfx
	if audio.expDelay < 0 then
    if not(isAudioEffectsOff) then
  		audio.expRot = audio.expRot + 1
  		if audio.expRot == 4 then audio.expRot = 1 end
  		audio.SoundFx[audio.expRot]:stop()
  		audio.SoundFx[audio.expRot]:setVolume(0.2 + math.random(3)/10)
  		audio.SoundFx[audio.expRot]:setPitch(0.70 + math.random()/40*2) -- one octave lower
  		audio.SoundFx[audio.expRot]:play()
  		audio.expDelay = math.random()*0.5 + 0.1
    end
	end
  ]]--
end

function audioExplosion()
  if not(isAudioEffectsOff) then
    audio.expRot = audio.expRot + 1
    if audio.expRot == 4 then audio.expRot = 1 end
    audio.SoundFx[audio.expRot]:stop()
    audio.SoundFx[audio.expRot]:setVolume(0.5)
    audio.SoundFx[audio.expRot]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
    audio.SoundFx[audio.expRot]:play()
  end
end

-- center camera to x, y by calculating correct scrolloffset
-- except when close to universe boundaries
function calculateOffsets(x, y)

	local screenwidth = (love.graphics.getWidth() / tv("scale"))
	local screenheight = (love.graphics.getHeight() / tv("scale"))
	local midpointx = - scrolloffsetX + (screenwidth  / 2)
	local midpointy = - scrolloffsetY + (screenheight / 2)

	-- so the delta to move scrolloffset is the difference between where the ship is drawn and the midpoint
	local deltax = midpointx - x
	local deltay = midpointy - y

	-- calculate distance from universe edge
	local xdistance = universe.width - midpointx
	local ydistance = universe.height - midpointy

	-- determine if ship coordinates are near boundary or in the middle - this affects scrolling
	-- values as strings "start", "mid", "end"
	local xarea = "mid"
	local yarea = "mid"
	if x < (screenwidth / 2) then xarea = "start" end
	if x > (universe.width - (screenwidth / 2)) then xarea = "end" end
	if y < (screenheight / 2) then yarea = "start" end
	if y > (universe.height - (screenheight / 2)) then yarea = "end" end

	-- in mid area, scroll freely
	if xarea == "mid" then scrolloffsetX = scrolloffsetX + deltax end
	if yarea == "mid" then scrolloffsetY = scrolloffsetY + deltay end

	-- if close to zero, that is "start", do nothing, offset remains put

	-- if close to end of universe boundary, that is "end" calculate correct offset
	-- it's universe boundary - screenwidth/height
	if xarea == "end" then scrolloffsetX = - (universe.width - screenwidth) end
	if yarea == "end" then scrolloffsetY = - (universe.height - screenheight) end

  if not(tv("scale") >= (love.graphics.getWidth() / universe.width)) then
		-- if scale is so zoomed out that the universe width is smaller in width than the screen, center it
		local actualWidthOfUniverse = universe.width*tv("scale")
		local centerXDelta = (love.graphics.getWidth() - actualWidthOfUniverse)/2
		scrolloffsetX = centerXDelta/tv("scale")
	end

end

-- params.eter turret is a string "ST", "RFT" ... etc.
function generateTurret(x, y, turret)
	local w_id = inventory:returnIndex(turret)
	local isRoom = true
	local i
	for i=1, #turrets do
		if CheckCollision(ship.x-20, ship.y-20, 40, 40, turrets[i].x-80, turrets[i].y-80, 160, 160) then isRoom = false end
	end
	if isRoom then
		inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1
		table.insert(turrets, Turret:new({
			x = x,
			y = y,
			angle = math.pi/2,
			type = turret}))
	else
		textLogger:newMessage("Too close to existing turret!", "green")
	end
end

function generateWeaponUpgrade()
  table.insert(itemPacks, Itempack:new({
    itemId = "RFCI",
    quantity = 1,
    x = love.graphics.getWidth(),
    y = love.graphics.getHeight()-60}))
end

function generateItemPack()
  local item = "DIM"
  local r, q = math.random(100), math.random(4)+4
  if r > 20 then
    item = "DIR"
    q = math.random(4)+4
  end
  if r > 40 then
    item = "RFT"
    q = 1
  end
  if r > 65 then
    item = "RGT"
    q = 1
  end
  if r > 90 then
    item = "RLT"
    q = 1
  end

	table.insert(itemPacks, Itempack:new({
		itemId = item,
    quantity = q,
		x = love.graphics.getWidth(),
		y = love.graphics.getHeight()-60}))
end


function generateAsteroid(x, y, type)
	table.insert(asteroids, Asteroid:new({
		type = type,
		x = x,
		y = y}))
end

function createFlashtext(x, y, msg, col, type)
	table.insert(flashTexts, Flashtext:new({
		duration = 100,
		colour = col,
		type = type,
		msg = msg,
		x = x,
		y = y}))
end

function createRailGunBlast(x, y, targetx, targety)
	-- check here if the line intersects with walls
	-- we iterate along the path step by step by step and check if the point collides with the walls
	-- and if yes, then move targetx and targety to that point and return false, if the whole iteration goes through then it's fine to blast all the way

	-- calculate the angle of the line
  local angle = - math.atan2(targetx - x, targety - y) + math.pi/2
	-- calculate original length of the line
	local length = math.sqrt((targetx - x)^2+(targety - y)^2)

	-- iterate istep until gone over the original length of the line
	local istep = 0 -- step iterator
	local ix = x -- x coordinate to check
	local iy = y -- y coordinate to check
	repeat
		istep = istep + 5
		ix = ix + math.cos(angle)*5
		iy = iy + math.sin(angle)*5
		-- check step by step along the line, collision with all walls (asteroids)
		for i = 1, #asteroids do
			if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ix-2, iy-2, 4, 4) then
				targetx = ix
				targety = iy
				table.insert(railGunBlasts, RailGunBlasts:new({
					targetX = targetx,
					targetY = targety,
					x = x,
					y = y}))
				return false
			end
		end
	until istep > length

	table.insert(railGunBlasts, RailGunBlasts:new({
		targetX = targetx,
		targetY = targety,
		x = x,
		y = y}))
	return true
end

function removeOldRailGunBlasts()
	for i, blast in ipairs(railGunBlasts) do
		if blast.removeMe then
			table.remove(railGunBlasts, i)
		end
	end
end

function s.resetGame()
  s.isPaused = false
  s.isControlsDisabled = false
  isStartGame = false -- startgame state is obsolete and could be cleaned out
  breach = false
  isSpawningPaused = false
  spawningPauseStamp = 0 -- use timestamp to count 5 seconds pause in spawning enemies between waves
  timeUntilNextWave = 0
  enemyWaveSize = 3000
  enemiesLeftInWave = 3000
  nextItemPackCounter = 0
  nextItemPackLimit = 200
  nextItemPackLimitGrowth = 50
  cameraOnShip = true
  scrolloffsetX = 0
	scrolloffsetY = 0
  weaponUpgradeIterator = 0

  ship = Ship:new({
		x = universe.width / 2,
		y = universe.height / 2,
		angle = math.pi*1.5,
	  speed = 0})

	swarmPathsHandler = SwarmPath:new()

	textLogger = Textlogger:new({
		maxrows = 3,
	  rowheight = 50,
		textsize = 24,
	  updateSpeed = 3, -- seconds (how often log scrolls on it's own)
	  x = 50,
	  y = love.graphics.getHeight() - 100 - 3*70,
		blinkDuration = 0.050, -- milliseconds how quickly new message blinks
	  maxBlinks = 3})

	weaponConsole = Weaponconsole:new({
		x = love.graphics.getWidth()/2 - 70,
	  y = love.graphics.getHeight() - 100})

  inventory = Inventory:new({})
  inventory:initiateInventory()

	bullets = {}
	enemies = {}
	turrets = {}
	asteroids = {}
	railGunBlasts = {}
	surrrges = {}
	explosions = {}
	smokes = {}
	mines = {}
	rockets = {}
  gameEvents = {}
  flashTexts = {}
  itemPacks = {}

  enemySpawners = {}
  -- generate first spawner
	table.insert(enemySpawners, Enemyspawner:new({
		pathId = 1}))
  enemySpawners[1].velocity = 80 -- let's make things move in the beginning
  spawnersOnPath = {}
  spawnersOnPath[1] = 1
  spawnersOnPath[2] = 0
  spawnersOnPath[3] = 0
  ySpeedIncrease = 0

	radarOn = true
	drawnEnemyCoords = {}
  kills = 0
  credits = 0

  -- controls for enemySpawner acceleration
  enemySpawnerAccelerationInterval = 50 -- 100
  enemySpawnerIterator = 0

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 12)
  msg = love.graphics.newText(font, "")
  local font = love.graphics.newFont("graphics/Krungthep.ttf", 18)
  msg2 = love.graphics.newText(font, "")

  generateAsteroid(universe.width/2-2800, universe.height/2-1500, "heavy")
  generateAsteroid(universe.width/2-2200, universe.height/2-1500, "heavy")

  generateAsteroid(universe.width/2+1000, universe.height/2+1000, "heavy")
  generateAsteroid(universe.width/2+1600, universe.height/2+1000, "heavy")

  generateAsteroid(universe.width/2-600, universe.height/2+5900, "heavy")
  generateAsteroid(universe.width/2, universe.height/2+5900, "heavy")
  generateAsteroid(universe.width/2+600, universe.height/2+5900, "heavy")

  gameStates.gameover.initiated = false
  gameStates.getready.initiated = false

  weaponConsole:switchWeapon()

  isAudioEffectsOff = false
  isAudioMusicOff = false
  audio.Music[2]:stop()
  audio.Music[1]:stop()
  audio.Music[1]:setVolume(0.3)
  audio.Music[1]:setLooping(true)
  audio.Music[1]:play()

end



function gameOver(reason)
  if not(currentSubState == "gameover") then
    local score = kills
    local text, text2 = "", ""

    currentSubState = "gameover"
    s.isControlsDisabled = true
    isAudioEffectsOff = true

    if reason == "death" then text2 = "GAME OVER" end
    if reason == "breach" then text2 = "ENEMY BREACH - GAME OVER" end

    if score <= s.highscore then
      text = text .. "HIGH SCORE REMAINS " .. s.highscore .. "\n\n"
    else
      s.highscore = score
      text = text .. "NEW HIGH SCORE\n\n"
    end
    text = text .. score .. "\n\n\n\nPRESS SPACE TO RESTART"
    gameStates.gameover.message = text
    gameStates.gameover.message2 = text2

    audio.Music[1]:stop()

    audio.Music[2]:stop()
--    audio.Music[2]:setVolume(0.5) -- volume is determined if player has toggled music on or off
    audio.Music[2]:setLooping(true)
    audio.Music[2]:play()

  end
end
