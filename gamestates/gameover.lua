gameStates.gameover = {}
gameStates.gameover.initiated = false

function gameStates.gameover.initiate()
  local font = love.graphics.newFont("graphics/Krungthep.ttf", 48)
  gameStates.gameover.Text = love.graphics.newText(font, "")
  font = love.graphics.newFont("graphics/Krungthep.ttf", 96)
  gameStates.gameover.Text2 = love.graphics.newText(font, "")
end

function gameStates.gameover.draw()
  gameStates.gameover.Text:clear()
  drawOverlay() -- defined in maingame.lua
  local textWidth = 2000
  local screenwidth = love.graphics.getWidth()
  local centerX = (screenwidth - textWidth)/2
  local foo = gameStates.gameover.Text2:addf(gameStates.gameover.message2, textWidth, "center", 0, 0, 0)
  foo = gameStates.gameover.Text:addf(gameStates.gameover.message, textWidth, "center", 0, 0, 0)
  love.graphics.setColor(255, 0, 200)
  love.graphics.draw(gameStates.gameover.Text2, centerX, 500)
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(gameStates.gameover.Text, centerX, 700)
end

function gameStates.gameover.keypressed(key)
  if key == "space" then
    currentSubState = "none"
    s.isPaused = false
    s.resetGame()
  end
end

function gameStates.gameover.update(dt)
  if not gameStates.gameover.initiated then
    gameStates.gameover.initiated = true
    gameStates.gameover.initiate()
  end
end
