function initiateLevel(lvl)
  if lvl == 1 then
    generateAsteroid(universe.width/2-2800, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-2200, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-1600, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-1000, universe.height/2-900, "heavy")
    generateAsteroid(universe.width/2+1000, universe.height/2-900, "heavy")
    generateAsteroid(universe.width/2+1600, universe.height/2-900, "heavy")

    function triggerFunction()
      return timeInState > 0
    end
    function payloadFunction()
      createFlashtext(1150, 840, "GET READY!", "green", "combat")
    end
    table.insert(gameEvents, Gameevent:new({
      trigger = triggerFunction,
      payload = payloadFunction}))


    function triggerFunction()
      return kills >= 0 and not(isStartGame)
    end
    function payloadFunction()
      createSurge(1, 2000, 1)
    end
    table.insert(gameEvents, Gameevent:new({
      trigger = triggerFunction,
      payload = payloadFunction}))

    function triggerFunction()
      return kills >= 1000 and not(isStartGame)
    end
    function payloadFunction()
      createSurge(1, 2000, 1)
      createSurge(2, 2000, 1)
      createSurge(3, 2000, 1)
    end
    table.insert(gameEvents, Gameevent:new({
      trigger = triggerFunction,
      payload = payloadFunction}))

    -- text tweens when entering level
    tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()/2, love.graphics.getHeight()-100, 0.5, easeOutQuint)
    tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
  end
end

function createSurge(intensity, amount, pathId)
  if intensity == 1 then
    table.insert(surrrges, Surrrges:new({
      x = 500,
      totalEnemies = amount,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.03,
      speed = 500,
      pathId = pathId}))
  elseif intensity == 2 then
    table.insert(surrrges, Surrrges:new({
      x = 500,
      totalEnemies = amount,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 500,
      pathId = pathId}))
  elseif intensity == 3 then
    table.insert(surrrges, Surrrges:new({
      x = 500,
      totalEnemies = amount,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 600,
      pathId = pathId}))

  end

end
