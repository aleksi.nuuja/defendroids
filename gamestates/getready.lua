gameStates.getready = {}
gameStates.getready.initiated = false

function gameStates.getready.initiate()
  local font = love.graphics.newFont("graphics/Krungthep.ttf", 48)
  gameStates.getready.Text = love.graphics.newText(font, "")
end

function gameStates.getready.draw()

  drawOverlay() -- defined in maingame.lua
  local textWidth = 2000

  local screenwidth = love.graphics.getWidth()
  local centerX = (screenwidth - textWidth)/2

  local text = "DEFEND THE BASE!\n\nTHOUSANDS OF ENEMY DRONES ARE APPROACHING DIRECTLY FROM ABOVE.\n\n\n\nUSE YOUR WEAPONS AND DEPLOY TURRETS TO REPEL THEIR ATTACK.\nSHOOT DOWN DRONES TO GET MORE TURRETS AND WEAPON UPGRADES.\n\nCONTROLS:       A-S-W-D       O - P\nR - TOGGLE RADAR\nM - TOGGLE MUSIC\n1, 2 - CAMERA ZOOM\n\nGOOD LUCK!\n\n\n\nPRESS SPACE TO CONTINUE"
  local foo = gameStates.getready.Text:addf(text, textWidth, "center", 0, 0, 0)
  love.graphics.setColor(255, 255, 255)
  love.graphics.draw(gameStates.getready.Text, centerX, 300)
end

function gameStates.getready.keypressed(key)
  if key == "space" then
    currentSubState = "none"
    s.isPaused = false
  end
end

function gameStates.getready.update(dt)
  if not gameStates.getready.initiated then
    gameStates.getready.initiated = true
    gameStates.getready.initiate()
  end
end
