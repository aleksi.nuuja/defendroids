Menu = {}

function Menu:new(params)
  o = {}
  o.x = params.x
  o.y = params.y
  o.type = params.type

  o.ItemsNorm = {}
  o.ItemsHigh = {}
  o.initiated = false
  o.shopItem = 1
  o.selected = 1
  if o.type == "shop" then o.selected = 5 end

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 22)
  o.msg = love.graphics.newText(font, "")
  o.msg2 = love.graphics.newText(font, "")
  o.msg3 = love.graphics.newText(font, "")
  o.msg4 = love.graphics.newText(font, "")

  setmetatable(o, self)
  self.__index = self
  return o
end

function Menu:draw()
  local i
  love.graphics.setColor(255, 255, 255)

  if self.type == "main" then
    for i=1,#self.ItemsNorm do
      if self.selected == i then
        love.graphics.draw(self.ItemsHigh[i], self.x, self.y + ((i-1)*self.ItemsHigh[i]:getHeight()))
      else
        love.graphics.draw(self.ItemsNorm[i], self.x, self.y + ((i-1)*self.ItemsHigh[i]:getHeight()))
      end
    end
  elseif self.type == "shop" then

    -- draw selected inventory item: icon, name, description, price
    self.msg:clear()
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(inventory.Items[self.shopItem].icon, 1000, 217)
    local text = inventory.Items[self.shopItem].name .. " - " .. inventory.Items[self.shopItem].price .. "Cr \n" .. inventory.Items[self.shopItem].descr
    local index = self.msg:addf(text, 450, "left", 0, 0, 0, 1, 1)
    love.graphics.setColor(0, 200, 0)
    love.graphics.draw(self.msg, 1110, 217)

    -- draw selected inventory item category
    self.msg2:clear()
    text = "CATEGORY:  " .. inventory.Items[self.shopItem].category
    index = self.msg2:addf(text, 450, "left", 0, 0, 0, 1, 1)
    love.graphics.setColor(0, 200, 0)
    love.graphics.draw(self.msg2, 1110, 150)

    -- draw text: how much credits you have
    self.msg3:clear()
    text = "CREDITS:  " .. gamestate.credits
    index = self.msg3:addf(text, 480, "left", 0, 0, 0, 1, 1)
    love.graphics.setColor(0, 200, 0)
    love.graphics.draw(self.msg3, 834, 440)

    -- draw text: current main weapon
    self.msg3:clear()
    text = "MAIN WEAPON:  " .. inventory.Items[inventory:returnIndex(inventory.mainWeapon)].id
    index = self.msg3:addf(text, 480, "left", 0, 0, 0, 1, 1)
    love.graphics.setColor(0, 200, 0)
    love.graphics.draw(self.msg3, 820, 680)


    -- draw icons to inventory what you already have and amount as number
    -- this should also check if saldo > 0 before drawing anything
    -- and this maps each i to a specific location, instead should increment location in steps if saldo found
    -- initial x = 1105, y = 482
    -- xstep = 129, ystep = 129 (5 x 2 matrix)

    local x = 1117
    local y = 482
    local xstep = 129
    local ystep = 129
    local xsteps = 0
    local ysteps = 0
    for i=1,#inventory.Items do
      if inventory.Saldos[i] > 0 then
        if not(inventory.Items[i].category == "Main weapon upgrades") then
          self.msg4:clear()
          love.graphics.setColor(255, 255, 255)
          love.graphics.draw(inventory.Items[i].icon, x, y)
          local text = inventory.Saldos[i]
          love.graphics.setColor(0, 0, 0)
          love.graphics.circle("fill", x+90, y+90, 30)
          local index = self.msg4:addf(text, 30, "center", 0, 0, 0, 1, 1)
          love.graphics.setColor(0, 255, 0)
          love.graphics.draw(self.msg4, x+75, y+75)
          if xsteps < 4 then
            xsteps = xsteps + 1
            x = x + xstep
          else
            x = x - 4 * xstep
            xsteps = 0
            ysteps = ysteps + 1
            y = y + ystep
          end
        end
      end
    end
    -- draw text: INVENTORY: INVENTORY: Select and press O to sell
    -- OR if non-empty inventory slot selected: INVENTORY: DIM selected, press O to sell for 500 cr


  end
end

function Menu:next()
  self.selected = self.selected + 1
  if self.selected > #self.ItemsHigh then
    self.selected = 1 end
end

function Menu:previous()
  self.selected = self.selected - 1
  if self.selected == 0 then self.selected = #self.ItemsHigh end
end

function Menu:previousShopItem()
  self.shopItem = self.shopItem - 1
  if self.shopItem == 0 then self.shopItem = #inventory.Items end
  if inventory.Items[self.shopItem].category == "Main weapon upgrades" then
    if not (inventory.Items[self.shopItem].id == inventory:returnNextWeapon()) then
      self:previousShopItem()
    end
  end
end

function Menu:nextShopItem()
  self.shopItem = self.shopItem + 1
  if self.shopItem > #inventory.Items then self.shopItem = 1 end
  if inventory.Items[self.shopItem].category == "Main weapon upgrades" then
    if not (inventory.Items[self.shopItem].id == inventory:returnNextWeapon()) then
      self:nextShopItem()
    end
  end
end

function Menu:buyItem(itemId)
  if gamestate.credits >= inventory.Items[itemId].price then
    inventory.Saldos[itemId] = inventory.Saldos[itemId] + 1
    gamestate.credits = gamestate.credits - inventory.Items[itemId].price
    if inventory.Items[itemId].category == "Main weapon upgrades" then
      inventory.Saldos[inventory:returnIndex(inventory.mainWeapon)] = 0
      inventory.mainWeapon = inventory.Items[itemId].id
      self:nextShopItem()
    end

    -- play sound effect for buying
    createFlashtext(1117, 440, "BOUGHT ONE ITEM. THANK YOU!", "green", "shop")
  else
    -- play sound effect for NOT ENOUGH MONEY
    createFlashtext(1117, 440, "NOT ENOUGH CREDITS!", "red", "shop")
  end

end

function Menu:sellItem(itemId)
  if inventory.Saldos[itemId] > 0 then
    inventory.Saldos[itemId] = inventory.Saldos[itemId] - 1
    gamestate.credits = gamestate.credits + inventory.Items[itemId].price

    -- play sound effect for buying
    createFlashtext(1117, 440, "SOLD. THANK YOU!", "green", "shop")
  end
end
