Asteroid = {}

function Asteroid:new(params)
  o = {}
  o.x = params.x
  o.y = params.y
  o.type = params.type
  o.xspeed = 0
  o.yspeed = 0
  o.height = asteroid:getHeight()
  o.width = asteroid:getWidth()

  if o.type == "heavy" then o.weight = 10 else o.weight = 4 end
  o.hitpoints = 255

  setmetatable(o, self)
  self.__index = self
  return o
end

function Asteroid:update(dt)

  -- note that asteroid gets speed only from impact
  self.x = self.x + self.xspeed
  self.y = self.y + self.yspeed

  -- universe boundaries
  if self.x < 0 then self.xspeed = 2  end
  if self.x + self.width > universe.width then self.xspeed = -2  end
  if self.y < 1000  then self.yspeed = self.yspeed + 100 end
  if self.y + self.height> universe.height then self.yspeed = -2 end

  -- motion slowing down
  self.xspeed = self.xspeed * (1-SLOWING*dt*self.weight)
  self.yspeed = self.yspeed * (1-SLOWING*dt*self.weight)

end


function Asteroid:draw(x, y, angle)
  if self.type == "light" then
    love.graphics.setColor(255, 200, 50, self.hitpoints)
  else
    love.graphics.setColor(255, 255, 255)
  end
  love.graphics.draw(asteroid, x, y)
end
