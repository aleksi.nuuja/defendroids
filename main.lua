require 'loadimages'
require 'tween2'
require 'audio'
require 'inventory'

function love.load()
	love.mouse.setVisible(false)
	math.randomseed(os.time())
	drawPathsDebug = false

	SHIPSIZE = 1 -- multiplier used in drawing ship and turrets, not enemies (should be obsolete!!!)
	SLOWING = 0.9999 -- 0.999 multiplier
	SCROLLSPEED = 1
	ZOOMDELTA = 0.9 -- factor (less than 1.0)
	SCROLLBOUNDARY = 0 -- distance from the edge when scrolling starts
	UNIVERSESIZE = 5
	MAXENEMIES = 2000 -- prevents lagging - this could be adaptive in proportion to dt
	SPAWNDELAY = 1 -- 0 is too fast, too many enemies will cause lag

	-- load all images
	loadAllImages() -- in loadimagelua

	universe = {
		width = bg:getWidth() * UNIVERSESIZE,
		height = bg:getHeight() * UNIVERSESIZE
	}

	tweenEngine = Tween:new()
  audio = Audio:new()
	initiateTweenValues()

	-- initiate state handling
	gameStates = {}
	stateTimeStamp = 0
	timeInState = 0
	gotoGameState("maingame")
	currentSubState = "none"
	require 'gamestates/maingame'
	-- substates
	require 'gamestates/getready'
	require 'gamestates/gameover'

	currentFPS = love.timer.getFPS()
	lowestFPS = 30
	highestFPS = 30
end

function gotoGameState(st)
	currentState = st
	stateTimeStamp = love.timer.getTime()
end

function initiateTweenValues()
	tweenEngine:newKeyAndValue("scale", love.graphics.getWidth() / universe.width)
	tweenEngine:newKeyAndValue("textSize", 5)
	tweenEngine:newKeyAndValue("textY", love.graphics.getHeight()-80)
	tweenEngine:newKeyAndValue("alpha", 255)
	tweenEngine:newKeyAndValue("logTextY", 0)
	tweenEngine:newKeyAndValue("logAlpha", 0)
	tweenEngine:newKeyAndValue("WCTextX", 0)
	tweenEngine:newKeyAndValue("WCAlpha", 0)
	tweenEngine:newKeyAndValue("musicVolume", 0)
	tweenEngine:newKeyAndValue("cameraY", 0)
	tweenEngine:newKeyAndValue("creditsMeter", 0)
	tweenEngine:newKeyAndValue("itemPackScale", 0.5)
	tweenEngine:newKeyAndValue("itemPackAlpha", 255)
end


function love.keypressed(key)
	if key == "escape" then -- this is global to all states
		print("Lowest FPS: " .. lowestFPS)
		print("Highest FPS: " .. highestFPS)
		love.event.quit()
--	elseif key == "z" then -- USE D FOR DEBUGGING BUTTONS
--		generateItemPack()
	elseif key == "m" then -- toggle music
		if not(isAudioMusicOff) then
			isAudioMusicOff = true
			audio.Music[1]:setVolume(0)
			audio.Music[2]:setVolume(0)
		else
			isAudioMusicOff = false
			audio.Music[1]:setVolume(0.3)
			audio.Music[2]:setVolume(0.5)
		end
	else
		if currentSubState == "none" then
			gameStates[currentState].keypressed(key)
		else -- if in substate, it overrides key input from main gamestate
			gameStates[currentSubState].keypressed(key)
		end
	end
end

function love.draw()
	gameStates[currentState].draw()
	if not(currentSubState == "none") then -- if in substate, draw both main gamestate and substate
		gameStates[currentSubState].draw()
	end
--	love.graphics.print("Time in current state: " .. timeInState, 10, 50)
end

function love.update(dt)
	-- track FPS
	currentFPS = love.timer.getFPS()
	if currentFPS < lowestFPS then lowestFPS = currentFPS end
	if currentFPS > highestFPS then highestFPS = currentFPS end

	tweenEngine:update(dt)
	audio:update(dt)

	timeInState = love.timer.getTime() - stateTimeStamp
	gameStates[currentState].update(dt)
	if not(currentSubState == "none") then -- if in substate, update both main gamestate and substate
		gameStates[currentSubState].update(dt)
	end
end
