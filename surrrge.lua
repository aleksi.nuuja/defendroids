Surrrges = {}

function Surrrges:new(params)
  o = {}
  o.initiated = false
  o.totalEnemies = params.totalEnemies
  o.spread = params.spread
  o.surgespeed = params.surgespeed
  o.speed = params.speed
  o.pathId = params.pathId

  o.delay = 0
  o.remaining = 0
  o.removeMe = false

  setmetatable(o, self)
  self.__index = self
  return o
end

function Surrrges:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateSurrrges()
  end

  if self.remaining <= 0 then
    self.removeMe = true
    return
  end

  self.delay = self.delay - dt
  if self.delay <= 0 then
    self.delay = self.surgespeed
    -- generate enemies around the first point of their path
    local startX = swarmPathsHandler.Paths[self.pathId][1] + (math.random() - 0.5) * self.spread * 5
    self:generateEnemy(startX, self.pathId, self.spread, self.speed)
  end
end

function Surrrges:initiateSurrrges()
  self.delay = self.surgespeed
  self.remaining = self.totalEnemies
end

function Surrrges:generateEnemy(x, pathId, spread, speed)
  local s = gameStates.maingame
  if epicEnd then
    -- at epic end if maximum enemies is reached, enemies stop surging until their number drops to half, then they continue
    -- this prevents the "flatness" of enemies appearing at same rate as they are destroyed which is boring
    if #enemies == MAXENEMIES then tempMaxEnemies = MAXENEMIES/2 end
    if #enemies == MAXENEMIES/2 then tempMaxEnemies = MAXENEMIES end
    if #enemies < tempMaxEnemies then
      self.remaining = self.remaining - 1
  		table.insert(enemies, Enemy:new({
  			x = x,
  			pathId = pathId,
        spread = spread,
  			speed = speed}))
    end
  elseif #enemies < MAXENEMIES then
    self.remaining = self.remaining - 1
		table.insert(enemies, Enemy:new({
			x = x,
			pathId = pathId,
      spread = spread,
			speed = speed}))
  end
end
