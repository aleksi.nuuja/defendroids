Bullet = {}

function Bullet:new(params)
  o = {}
  o.speed = params.speed
  o.dir = params.dir
  o.x = params.x
  o.y = params.y
  o.startx = params.x
  o.starty = params.y
  o.maxrange = params.maxrange
  o.xspeed = math.cos(o.dir) * o.speed
  o.yspeed = math.sin(o.dir) * o.speed
  o.type = params.type

  o.removeMe = false

  setmetatable(o, self)
  self.__index = self
  return o
end

function Bullet:draw()

  local size = 0.2
  if self.type == 2 then size = 0.1 end

	local sidepointleftX = self.x + math.cos(self.dir+(1.57079633+0.5))*10*size
	local sidepointleftY = self.y + math.sin(self.dir+(1.57079633+0.5))*10*size
	local sidepointrightX = self.x + math.cos(self.dir-(1.57079633+0.5))*10*size
	local sidepointrightY = self.y + math.sin(self.dir-(1.57079633+0.5))*10*size

  love.graphics.setColor(255, 255, 255)

	-- lines from back to sidepoints
	love.graphics.line(((self.x-(math.cos(self.dir))*30*size)),((self.y-(math.sin(self.dir))*30*size)),sidepointleftX,sidepointleftY)
	love.graphics.line(((self.x-(math.cos(self.dir))*30*size)),((self.y-(math.sin(self.dir))*30*size)),sidepointrightX,sidepointrightY)

	-- lines from sidepoints to fwdpoint
	love.graphics.line(sidepointleftX, sidepointleftY,((self.x+(math.cos(self.dir))*640*size)),((self.y+(math.sin(self.dir))*640*size)))
	love.graphics.line(sidepointrightX, sidepointrightY,((self.x+(math.cos(self.dir))*640*size)),((self.y+(math.sin(self.dir))*640*size)))
end

function Bullet:update(dt)
    self.x = self.x + self.xspeed * dt
    self.y = self.y + self.yspeed * dt

    -- calculate distance travelled
    local distance = math.sqrt((self.x-self.startx)*(self.x-self.startx)+(self.y-self.starty)*(self.y-self.starty))
    if distance > self.maxrange then self.removeMe = true end

    -- universe boundaries
    if (self.x < -10) or (self.x > universe.width + 10)
    or (self.y < -10) or (self.y > universe.height + 10) then
      self.removeMe = true
    end
end
