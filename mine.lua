Mine = {}

function Mine:new(params)
  o = {}
  o.initiated = false
  o.removeMe = false
  o.x = params.x
  o.y = params.y
  o.xspeed = params.xspeed
  o.yspeed = params.yspeed
  o.type = params.type -- if omitted, default to "normal"
  o.angle = params.angle
  o.targetAngle = params.targetAngle -- projectiles have a target angle which they steer towards

  o.speed = math.sqrt(o.xspeed * o.xspeed + o.yspeed * o.yspeed)

  o.angleAdjustment = 0.001 -- how fast the projectile steers towards target angle
--  o.angle =   math.atan2(o.yspeed, o.xspeed)


  setmetatable(o, self)
  self.__index = self
  return o
end

function Mine:draw(x, y, angle)
  local pi = math.pi
  local size = 0.8
  if not(self.type == "normal") then size = 0.6 end
  angle = math.random() * math.pi*2

  e = {(x + math.cos(angle+(pi/2))*20*size) + (math.cos(angle))*75*size, (y + math.sin(angle+(pi/2))*20*size) + (math.sin(angle))*75*size}
  f = {(x + math.cos(angle-(pi/2))*20*size) + (math.cos(angle))*75*size, (y + math.sin(angle-(pi/2))*20*size) + (math.sin(angle))*75*size}

  g = {(x + math.cos(angle+(pi/2))*20*size) - (math.cos(angle))*30*size, (y + math.sin(angle+(pi/2))*20*size) - (math.sin(angle))*30*size}
  h = {(x + math.cos(angle-(pi/2))*20*size) - (math.cos(angle))*30*size, (y + math.sin(angle-(pi/2))*20*size) - (math.sin(angle))*30*size}

  i = {(x + math.cos(angle-(pi/2))*40*size) + (math.cos(angle+pi))*15*size, (y + math.sin(angle-(pi/2))*40*size) + (math.sin(angle+pi))*15*size}
  j = {(x + math.cos(angle+(pi/2))*40*size) + (math.cos(angle+pi))*15*size, (y + math.sin(angle+(pi/2))*40*size) + (math.sin(angle+pi))*15*size}

	love.graphics.setColor(0, 0, 0) -- black line
	love.graphics.setLineWidth(10)
	love.graphics.circle('line', x, y, 55*size)
  love.graphics.setColor(150, 150, 150) -- colour fill
	love.graphics.circle('fill', x, y, 55*size)

  love.graphics.setColor(0, 0, 0) -- black line
  love.graphics.setLineWidth(10)
	love.graphics.circle('line', x, y, 40*size)
  love.graphics.setColor(150, 150, 150) -- colour fill
	love.graphics.circle('fill', x, y, 40*size)

	love.graphics.setColor(0, 0, 0) -- black line
  love.graphics.setLineWidth(10)
	love.graphics.polygon('line', g[1], g[2], j[1], j[2], e[1], e[2])
  love.graphics.polygon('line', h[1], h[2], i[1], i[2], f[1], f[2])

  love.graphics.setColor(150, 150, 150) -- colour fill
  love.graphics.polygon('fill', g[1], g[2], j[1], j[2], e[1], e[2])
  love.graphics.polygon('fill', h[1], h[2], i[1], i[2], f[1], f[2])
end


function Mine:update(dt)

  self.x = self.x + self.xspeed
  self.y = self.y + self.yspeed

  if self.type == "normal" then
    self.angle = math.random() * math.pi*2 -- normal mines just whirl around

    -- universe boundaries
    if self.x < 40 then self.xspeed = 1  end
    if self.x > universe.width-40 then self.xspeed = -1  end
    if self.y < 40  then self.yspeed = 1 end
    if self.y > universe.height-40 then self.yspeed = -1 end

    -- motion slowing down
    self.xspeed = self.xspeed * (1-SLOWING*2*dt)
    self.yspeed = self.yspeed * (1-SLOWING*2*dt)
  else -- remove projectile mines at universe boundaries
    if self.x < 40 then self.removeMe = true  end
    if self.x > universe.width-40 then self.removeMe = true end
    if self.y < 40  then self.removeMe = true end
    if self.y > universe.height-40 then self.removeMe = true end
  end

  if self.type == "projectile" then -- steer towards target angle
    if self.targetAngle < self.angle then
      self.angle = self.angle - self.angleAdjustment
      self.xspeed = math.cos(self.angle) * self.speed
      self.yspeed = math.sin(self.angle) * self.speed
    elseif self.targetAngle > self.angle then
      self.angle = self.angle + self.angleAdjustment
      self.xspeed = math.cos(self.angle) * self.speed
      self.yspeed = math.sin(self.angle) * self.speed
    end
  end
end
