RailGunBlasts = {}

function RailGunBlasts:new(params)
  o = {}
  o.x = params.x
  o.y = params.y
  o.targetX = params.targetX
  o.targetY = params.targetY
  o.brightness = 255
  o.removeMe = false

  setmetatable(o, self)
  self.__index = self
  return o
end

function RailGunBlasts:draw()
  love.graphics.setColor(255, 255, 255, self.brightness) -- colour fill
  love.graphics.setLineWidth((255-self.brightness)/2+1)
  love.graphics.line(self.x, self.y, self.targetX, self.targetY)
end

function RailGunBlasts:update(dt)
  self.brightness = self.brightness - 50

  if self.brightness <= 0 then self.removeMe = true end
end
