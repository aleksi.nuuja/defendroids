Itempack = {}

function Itempack:new(params)
  o = {}
  o.x = params.x
  o.y = params.y
  o.itemId = params.itemId
  o.quantity = params.quantity
  o.initiated = false
  o.animateMe = false
  o.removeMe = false
  o.animTimeStamp = 0

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 20)
  o.msg = love.graphics.newText(font, "")

  setmetatable(o, self)
  self.__index = self
  return o
end

function Itempack:draw()
  if self.initiated then
    local w_id = inventory:returnIndex(self.itemId)

    if self.animateMe then
      love.graphics.setColor(0, 0, 0, tv("itemPackAlpha"))
      love.graphics.circle("fill", self.x+(48*tv("itemPackScale")), self.y+(48*tv("itemPackScale")), 90*tv("itemPackScale"))
      love.graphics.setColor(255, 255, 255, tv("itemPackAlpha"))
      love.graphics.draw(inventory.Items[w_id].icon, self.x, self.y, 0, tv("itemPackScale"), tv("itemPackScale"))
    else
      love.graphics.setColor(0, 0, 0)
      love.graphics.circle("fill", self.x+24, self.y+24, 45)
      love.graphics.setColor(255, 255, 255)
      love.graphics.draw(inventory.Items[w_id].icon, self.x, self.y, 0, 0.5, 0.5)

      self.msg:clear()
      local text = ""
      text = self.quantity
      love.graphics.setColor(0, 0, 0)
      love.graphics.circle("fill", self.x-5, self.y-5, 20)
      local index = self.msg:addf(text, 30, "center", 0, 0, 0)
      love.graphics.setColor(0, 255, 0)
      love.graphics.draw(self.msg, self.x-20, self.y-18)
    end
  end
end

function Itempack:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateItempack()
  else
    self.x = self.x - 2 -- item packs scroll from right to left
    if self.x <= love.graphics.getWidth()/2 then
      if not(self.animateMe) then
        if not (self.itemId == "RFCI") then
          local w_id = inventory:returnIndex(self.itemId)
          inventory.Saldos[w_id] = inventory.Saldos[w_id] + self.quantity
        else -- is a weaponupgrade!
          createFlashtext(ship.x, ship.y, "WEAPON UPGRADE!", "green", "max")
          local nextMainWeapon = inventory:returnNextWeapon()
          w_id = inventory:returnIndex(nextMainWeapon)
          inventory.Saldos[inventory:returnIndex(inventory.mainWeapon)] = 0
          inventory.Saldos[w_id] = 1
          inventory.mainWeapon = inventory.Items[w_id].id
          weaponConsole:switchWeapon()
        end
        self.animateMe = true
        self.animTimeStamp = love.timer.getTime()
        tweenEngine:createTween("itemPackScale", 0.5, 2, 0.5, linearTween)
        tweenEngine:createTween("itemPackAlpha", 255, 0, 0.5, linearTween)
      else
        local animTimer = love.timer.getTime() - self.animTimeStamp -- accuracy in milliseconds
        if animTimer >= 1 then
          self.removeMe = true
        end
      end
    end
  end
end

function Itempack:initiateItempack()
  -- initiateMe
end
