Explosion = {}

function Explosion:new(params)
  o = {}
  o.initiated = false
  o.removeMe = false
  o.explosionState = 0 -- this is used both to display animation frames and to scale the bounding box
  o.x = params.x
  o.y = params.y
  o.angle = params.angle
  o.size = params.size
  o.killcounter = 0

  -- each explosion has a bounding box that collides with enemies and stuff
  o.boundingBoxX = o.x
  o.boundingBoxY = o.y
  o.boundingBoxWidth = 0
  o.boundingBoxHeight = 0

  setmetatable(o, self)
  self.__index = self
  return o
end

function Explosion:draw()
  if self.initiated then
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(bigexp[self.explosionState], self.x, self.y, self.angle, self.size, self.size, 128, 128) -- the explosion is 256 pixels in size

    -- DEBUGGING: Draw bounding box
--    love.graphics.setColor(0, 255, 0)
--    love.graphics.rectangle("line", self.boundingBoxX, self.boundingBoxY, self.boundingBoxWidth, self.boundingBoxHeight)

  end
end

function Explosion:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateExplosion()
  else
    self.explosionState = self.explosionState + 1

    -- scale the bounding box
    if self.explosionState < 35 then -- the last frames are just smoke and debris, remove destroying bounding box
      self.boundingBoxX = self.boundingBoxX - 2*self.size
      self.boundingBoxY = self.boundingBoxY - 2*self.size
      self.boundingBoxWidth = self.boundingBoxWidth + 4*self.size
      self.boundingBoxHeight = self.boundingBoxHeight + 4*self.size
    elseif self.explosionState == 36 then
      self.boundingBoxX = self.x
      self.boundingBoxY = self.y
      self.boundingBoxWidth = 0
      self.boundingBoxHeight = 0
      if self.killcounter > 50 then createFlashtext(self.x, self.y, self.killcounter .. " KILLS", "green", "combat") end
    end

  end

  if self.explosionState > 48 then
    self.removeMe = true


    return
  end

end

function Explosion:initiateExplosion()
  self.explosionState = 1
end
