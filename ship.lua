Ship = {}

function Ship:new(params)
  o = {}
	o.turnspeed = 5
	o.heat = 0 -- cannon heat
	o.heatp = 0.1 -- cannon heat initial value = lower value is higher rate of fire
  o.mheat = 0 -- mine projectile heat
	o.mheatp = 0.5 -- slower rate of fire
  o.soundId = 4
  o.xspeed, o.yspeed = params.speed, params.speed
  o.angle = params.angle -- direction as radians, zero being up
  o.x = params.x
	o.y = params.y

  o.hurt = false -- first collision will get you hurt and you start emitting smoke. if you are hurt and collide, you die
  o.smokecounter = 0
  o.smokefreq = 5
  o.hurtStamp = 0 -- timestamp for getting hurt, used for timing being indestructable
  o.indestructable = false
  o.indestructableSwitch = false

  o.alive = true
  o.deathStamp = 0 -- timestamp for death, used for timing explosions
  o.exploState = 0

  o.sprite = love.graphics.newImage("graphics/shipgimp1.png") -- 160 x 152 png

  setmetatable(o, self)
  self.__index = self
  return o
end

function Ship:draw()
  love.graphics.setColor(255, 255, 255)
  if self.alive then
    love.graphics.draw(self.sprite, self.x, self.y, self.angle+math.pi/2, 1, 1, 80, 76)
  end
end

function Ship:update(dt)
  s = gameStates.maingame
  self.x = self.x + self.xspeed -- not multiplied by dt ????
  self.y = self.y + self.yspeed -- not multiplied by dt ????

  -- universe boundaries
  if self.x < 40 then self.xspeed = 1  end
  if self.x > universe.width-40 then self.xspeed = -1  end
  if self.y < 40  then self.yspeed = 1 end
  if self.y > universe.height-40 then self.yspeed = -1 end

  -- motion slowing down
  self.xspeed = self.xspeed * (1-SLOWING*dt)
  self.yspeed = self.yspeed * (1-SLOWING*dt)

  -- got hit and is hurt. emit smoke
  if self.hurt and self.alive then
    if self.hurtStamp == 0 then self.hurtStamp = love.timer.getTime() end
    local timeHurt = love.timer.getTime() - self.hurtStamp -- accuracy in milliseconds

    if timeHurt < 0.100 and self.exploState == 0 then
      self.exploState = 1
      -- invoke explosion
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 3,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end

    if timeHurt > 1 and not(self.indestructableSwitch) then -- after one second, not indestructable anymore
      self.indestructableSwitch = true
      self.indestructable = false
      self.exploState = 0 -- reset exploState as it's also used for death explosions
    end

    self.smokecounter = self.smokecounter + 1
    if self.smokecounter > self.smokefreq then
      self.smokecounter = 0
      table.insert(smokes, Smoke:new({
        dir = self.angle,
        size = 15,
        x = self.x+(math.cos(self.angle-math.pi)*70),
        y = self.y+(math.sin(self.angle-math.pi)*70)}))
    end
  end

  -- destroyed! invoke 6 explosions
  if not self.alive then
    if self.deathStamp == 0 then self.deathStamp = love.timer.getTime() end
    local timeDead = love.timer.getTime() - self.deathStamp -- accuracy in milliseconds

    if self.exploState == 0 and timeDead > 0 then
      self.exploState = self.exploState + 1
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 3,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 1 and timeDead > 0.02 then
      self.exploState = self.exploState + 1
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 2,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 2 and timeDead > 0.04 then
      self.exploState = self.exploState + 1
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 2,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 3 and timeDead > 0.06 then
      self.exploState = self.exploState + 1
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 2,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 4 and timeDead > 0.08 then
      self.exploState = self.exploState + 1
      table.insert(explosions, Explosion:new({
        x = self.x + math.random()*50 - 25,
        y = self.y + math.random()*50 - 25,
        size = 2,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 5 and timeDead > 0.14 then
      self.exploState = self.exploState + 1
      -- last explosion is bigger
      table.insert(explosions, Explosion:new({
        x = self.x,
        y = self.y,
        size = 4,
        angle = math.random()*math.pi*2}))
      audioExplosion()
    end
    if self.exploState == 6 and timeDead > 2 then
      gameOver("death")
    end
  end
end
