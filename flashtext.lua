Flashtext = {}

function Flashtext:new(params)
  o = {}
  o.initiated = false
  o.removeMe = false
  o.x = params.x
  o.y = params.y
  o.msg = params.msg
  o.duration = params.duration
  o.colour = params.colour
  o.type = params.type
  if o.type == "combat" then
    o.size = 1
  elseif o.type == "max" then
      o.size = 2
  else
    o.size = 0.5
  end
  o.alpha = 255

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 48)
  o.Text = love.graphics.newText(font, "")

  setmetatable(o, self)
  self.__index = self
  return o
end

function Flashtext:draw()
  if self.initiated then
    if self.colour == "green" then
      love.graphics.setColor(0, 255, 0, self.alpha)
    elseif self.colour == "red" then
      love.graphics.setColor(255, 0, 150, self.alpha)
    end
    love.graphics.draw(self.Text, self.x, self.y, 0, self.size, self.size)
  end
end

function Flashtext:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateFlashtext()
  else
    self.FlashtextState = self.FlashtextState + 1
    local previousWidth = self.Text:getWidth() * self.size
    if self.type == "combat" or self.type == "max" then self.size = self.size * 1.01 end
    -- as size grows with multiplier 1.01, move x coordinate half of that amount to keep text centered
    local newWidth = self.Text:getWidth() * self.size
    local deltaX = (newWidth - previousWidth)/2
    self.x = self.x - deltaX

    local fadespeed = 2
    if self.type == "shop" then fadespeed = 4 end
    self.alpha = self.alpha - fadespeed
    if self.FlashtextState > self.duration then self.removeMe = true end
  end

end

function Flashtext:initiateFlashtext()
  self.FlashtextState = 1
  self.Text:clear()
  local foo = self.Text:set(self.msg)
end
