Item = {}

function Item:new(params)
  o = {}
  o.name = params.name
  o.id = params.id
  o.descr = params.descr
  o.category = params.category
  o.icon = params.icon
  o.price = params.price
  o.isTurret = params.isTurret
  o.allowedInStartgame = params.allowedInStartgame

  setmetatable(o, self)
  self.__index = self
  return o
end
