require 'menu'
require 'shopbutton'

Gamestate = {}

function Gamestate:new(params)
  o = {}
	o.level = 1
  o.kills = 0
  o.breach = false -- if enemies reach bottom they breach and game over
  o.credits = 500000 -- you can buy stuff with credits in shop state

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 12)
  o.msg = love.graphics.newText(font, "")

  -- states: mainmenu, shop, alert, getready, startgame, combat, defeat, victory, nextlevel
  -- special state: alert - displays message to player and "press space to continue"
  o.state = "mainmenu"
  o.substate = 0 -- substate increments by one with each state event and prevents events being called more than once
  o.startTime = os.time()
  o.stateDuration = 5
  o.timeRemain = 0
  o.turretsLeft = 0

  o.epicEnd = false -- when true, enemies surge in large batches
  o.tempMaxEnemies = 0 -- used in epic end to control amount of enemies

  setmetatable(o, self)
  self.__index = self
  return o
end

function Gamestate:draw()
  if self.state == "mainmenu"  then
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(mainmenutitle, love.graphics.getWidth()/6, love.graphics.getHeight()/4)
    if mainMenu.initiated then mainMenu:draw() end
  elseif self.state == "shop"  then
    local screenwidth = (love.graphics.getWidth() / tv("scale"))
    local centerX = (screenwidth - shopbg:getWidth())/2
    love.graphics.setColor(255, 255, 255, tv("alpha"))
    love.graphics.draw(shopbg, centerX, 0, 0)
    if shop.initiated then shop:draw() end
  elseif self.state == "alert"  then
    love.graphics.setColor(255, 255, 255, tv("alpha"))
    self.msg:clear()
    local index = self.msg:add("DEFEND THE BASE!\n\nAN ENEMY SWARM OF HUNDREDS IS APPROACHING DIRECTLY FROM ABOVE.\nLET A SINGLE ENEMY FIGHTER THROUGH, AND IT'S GAME OVER!\n\nFIRST YOU NEED TO DEPLOY YOUR TURRETS AND PUSH WALLS\nWHERE YOU WANT THEM.\n\nGOOD LUCK!\n\nPRESS SPACE TO CONTINUE", 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/2 - self.msg:getWidth()*2.5
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "getready"  then
    love.graphics.setColor(255, 255, 255, tv("alpha"))
    self.msg:clear()
    local index = self.msg:add("GET READY!", 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/2 - self.msg:getWidth()*tv("textSize")/2
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "startgame"  then
    love.graphics.setColor(255, 255, 255, tv("alpha"))
    self.msg:clear()
    if self.turretsLeft > 0 then local index = self.msg:add("DEPLOY - O           SWITCH - P", 0, 0, 0, tv("textSize"), tv("textSize"))
    else
      if self.timeRemain > 3 then local index = self.msg:add("PRESS SPACE TO CONTINUE", 0, 0, 0, tv("textSize"), tv("textSize"))
      else local index = self.msg:add("ENEMY ATTACK IMMINENT!", 0, 0, 0, tv("textSize"), tv("textSize")) end
    end
    local text_x = love.graphics.getWidth()/12
    love.graphics.draw(self.msg, text_x, tv("textY"))
    self.msg:clear()
    if self.timeRemain > 3 then
      local index = self.msg:add("PRESS SPACE WHEN READY", 0, 0, 0, tv("textSize"), tv("textSize"))
    else
      local index = self.msg:add("ENEMY ATTACK IMMINENT!", 0, 0, 0, tv("textSize"), tv("textSize"))
    end
    local text_x = love.graphics.getWidth()/12 * 8
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "combat" then
    love.graphics.setColor(255, 255, 255)
    self.msg:clear()
    local index = self.msg:add("ENEMYCOUNT:" .. #enemies, 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12
    love.graphics.draw(self.msg, text_x, tv("textY"))
    self.msg:clear()
    local index = self.msg:add("KILLS: " .. self.kills, 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/12 * 8
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "victory" then
    love.graphics.setColor(255, 255, 255)
    self.msg:clear()
    local index = self.msg:add("YOU WIN!            CREDITS EARNED: " .. math.round(tv("creditsMeter"), 0), 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/8
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "defeat" then
    love.graphics.setColor(255, 255, 255)
    self.msg:clear()
    local index = self.msg:add("GAME OVER\n\nPRESS SPACE TO RESTART LEVEL " .. self.level, 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/2 - self.msg:getWidth()*tv("textSize")/2
    love.graphics.draw(self.msg, text_x, tv("textY"))
  elseif self.state == "nextlevel" then
    love.graphics.setColor(255, 255, 255, tv("alpha"))
    self.msg:clear()
    local index = self.msg:add("LEVEL " .. self.level .. "\n\nPRESS SPACE TO CONTINUE", 0, 0, 0, tv("textSize"), tv("textSize"))
    local text_x = love.graphics.getWidth()/2 - self.msg:getWidth()*4
    love.graphics.draw(self.msg, text_x, tv("textY"))
  end
end

function Gamestate:update(dt)
  self.timeRemain = self.stateDuration - os.difftime(os.time(), self.startTime)

  -- events within a state and state changes

  if self.state == "mainmenu" and self.substate == 0 then
    self.substate = self.substate + 1
    initiateMainmenu()
  end

  if self.state == "mainmenu" and self.substate == 2 then
    self.substate = 0
    self:initiateState("shop", 0)
  end

  if self.state == "shop" and self.substate == 1 then
    self.substate = 0
    self:initiateState("alert", 0)
  end

  -- start at alert state: instructions to player and wait until space is pressed (in main.lua space-press is substate++)
  if self.state == "alert" and self.substate == 0 then
    self.substate = self.substate + 1
    tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()/2, love.graphics.getHeight()/2+200, 0.5, easeOutQuint)
    tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, 1, 0.5, easeOutQuint)
  end

  -- start game! move into state getready! setup level = create walls (asteroids) etc.
  if self.state == "alert" and self.substate == 2 then
    self:initiateState("getready", 4)
    self.substate = 0
    self:setupLevel(self.level)
    tweenEngine:createTween("musicVolume", 0, 0.3, 2, linearTween)
    audio.fadingInMusic = true
    audio.Music[2]:stop()
    audio.Music[2]:setVolume(0)
    audio.Music[2]:setLooping(true)
    audio.Music[2]:play()
    lowestFPS = 60 -- reset
  end

  -- get ready! tween in the text
  if self.state == "getready" and self.substate == 0 then
    self.substate = self.substate + 1
    tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()/2-200, love.graphics.getHeight()/2+100, 0.5, easeOutQuint)
  end

  -- tween out text - cool effectxz!
  if self.state == "getready" and self.timeRemain == 2 and self.substate == 1 then
    self.substate = self.substate + 1
    tweenEngine:createTween("alpha", 255, 0, 0.8, easeOutQuint)
    tweenEngine:createTween("textSize", 4, 20, 0.8, easeOutQuint)
  end

  -- move to startgame state -- deploy turrets
  -- if all turrets have been deployed, pressing space skips to 3 second waiting time (in main.lua)
  if self.state == "getready" and self.timeRemain == 1 and self.substate == 2 then
    self.substate = 0
    self:initiateState("startgame", 2400)
    self:initiateTextTweens()
    weaponConsole:switchWeapon()
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, love.graphics.getWidth() / universe.width, 1.5, easeInOutCubic)
  end

  -- go to combat - enemies attack, text shows enemy count
  if self.state == "startgame" and self.timeRemain < 0 then
    self.substate = 0
    self:initiateState("combat", 3)
    weaponConsole:switchWeapon()

    -- basic flow of enemies at first
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.03,
      speed = 500,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.03,
      speed = 500,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.03,
      speed = 500,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.03,
      speed = 500,
      pathId = math.random(2)}))


    self:initiateTextTweens()
    tweenEngine:createTween("musicVolume", 0, 0.2, 2, linearTween)
    audio.fadingInMusic = true
    audio.Music[2]:stop()
    audio.Music[1]:stop()
    audio.Music[1]:setVolume(0)
    audio.Music[1]:setLooping(true)
    audio.Music[1]:play()
  end

  -- second surge after 4000 kills, more intensive
  if self.state == "combat" and self.kills > 4000 and self.substate == 0 then
    self.substate = self.substate + 1
    textLogger:newMessage("WARNING: Enemy density increasing.", "red")
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 600,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 600,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 600,
      pathId = math.random(2)}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 1500, -- between 1500 and 2000 looks nice
      surgespeed = 0.01,
      speed = 600,
      pathId = math.random(2)}))
  end

  -- third surge after 12000 kills, epic end battle
  if self.state == "combat" and self.kills > 12000 and self.substate == 1 then
    self.substate = self.substate + 1
    self.epicEnd = true
    self.tempMaxEnemies = MAXENEMIES/2
    textLogger:newMessage("WARNING: Enemy altering their attack pattern.", "red")

    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 1}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 1}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 1}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 1}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 2}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 2}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 2}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 2}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 3}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2500, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 3}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 3}))
    table.insert(surrrges, Surrrges:new({
			x = x,
      totalEnemies = 2000,
      spread = 2000, -- between 1500 and 2000 looks nice
      surgespeed = 0,
      speed = 600+math.random(100),
      pathId = 3}))
  end

  -- BREACH! enemy reached the base and player is defeated - show game over and wait for space to restart level
  if self.state == "combat" and self.breach then
    self.substate = 0
    self:initiateState("defeat", 1)
    tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()-1000, love.graphics.getHeight()-500, 0.5, easeOutQuint)
    tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
  end

  -- restart level! go to shop first
  if self.state == "defeat" and self.substate == 1 then
    self:initiateState("shop", 0)
    tweenEngine:createTween("alpha", 0, 255, 0.1, easeOutQuint)
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, 1, 0.1, easeOutQuint)
    self.substate = 0
    self:setupLevel(self.level)
  end

  -- enemies defeated!! show win text for 5 seconds, then move to (shop) next level
  if self.state == "combat" and #enemies == 0 and self.timeRemain < 0 then
--    self.level = self.level + 1 -- in demo version, just loop level 1
    if self.level > 3 then self.level = 1 end -- for testing the existing levels we loop back to 1
    self:initiateState("victory", 5)
    tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()-300, love.graphics.getHeight()-130, 0.5, easeOutQuint)
    tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
    tweenEngine:createTween("creditsMeter", 0, self.credits, 3, linearTween)
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, love.graphics.getWidth()/universe.width*4, 0.5, easeOutQuint)
  end

  -- show alert state announcing next level and space to continue
  if self.state == "victory" and self.timeRemain < 0 then
    self.substate = 0
    self:initiateState("nextlevel", 0) -- this state remains until player presses space
    tweenEngine:createTween("textSize", 10, 6, 0.5, easeOutQuint)
    tweenEngine:createTween("textY", love.graphics.getHeight()/2, love.graphics.getHeight()/2+200, 0.5, easeOutQuint)
    tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, 1, 0.5, easeOutQuint)
  end

  -- start next level!
  -- should start with shop first
  if self.state == "nextlevel" and self.substate == 1 then
    self:initiateState("shop", 0)
    tweenEngine:createTween("alpha", 0, 255, 0.1, easeOutQuint)
    local currentScale = tv("scale")
    tweenEngine:createTween("scale", currentScale, 1, 0.1, easeOutQuint)
    self.substate = 0
    self:setupLevel(self.level)
  end

end

function Gamestate:initiateState(newstate, duration)
  self.substate = 0
  self.state = newstate
  self.startTime = os.time()
  self.stateDuration = duration
  self.timeRemain = duration

  if newstate == "startgame" then
    weaponConsole:switchWeapon()
  elseif newstate == "combat" then
    weaponConsole:switchWeapon()
  elseif newstate == "shop" then
    initiateShop()
    shop.shopItem = inventory:returnFirstTurret()

  end

end

function Gamestate:initiateTextTweens()
  tweenEngine:createTween("textSize", 20, 4, 0.5, easeOutQuint)
  tweenEngine:createTween("textY", love.graphics.getHeight()-300, love.graphics.getHeight()-70, 0.5, easeOutQuint)
  tweenEngine:createTween("alpha", 0, 255, 0.5, easeOutQuint)
end

function Gamestate:setupLevel(level)
  -- reset enemies, asteroids and turrets and other variables and stuff
  ENEMYWAVE = 0
  CAMERAONSHIP = true
  bullets = {}
	enemies = {}
	turrets = {}
	asteroids = {}
	railGunBlasts = {}
	surrrges = {}
	explosions = {}
	smokes = {}
	mines = {}
	rockets = {}

  self.epicEnd = false
  self.kills = 0
  self.breach = false -- if enemies reach bottom they breach and game over
  swarmPathsHandler:initiateSwarmPath()
--  inventory:initiateInventory()

  -- reset ship (could be a method for ship ...)
  ship.x = universe.width / 2
  ship.y = universe.height / 2
  ship.xspeed = 0
  ship.yspeed = 0
  ship.alive = true
  ship.deathStamp = 0
  ship.exploState = 0
  ship.hurt = false -- first collision will get you hurt and you start emitting smoke. if you are hurt and collide, you die
  ship.smokecounter = 0
  ship.hurtStamp = 0 -- timestamp for getting hurt, used for timing being indestructable
  ship.indestructable = false
  ship.indestructableSwitch = false


  if level == 1 then
    self.turretsLeft = 12 -- 6
    ENEMYDELAY = 5
    ENEMYSPEED = 500 -- 300

    generateAsteroid(universe.width/2-2800, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-2200, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-1600, universe.height/2-500, "heavy")
    generateAsteroid(universe.width/2-1000, universe.height/2-900, "heavy")
    generateAsteroid(universe.width/2+1000, universe.height/2-900, "heavy")
    generateAsteroid(universe.width/2+1600, universe.height/2-900, "heavy")


  elseif level == 2 then
    self.turretsLeft = 30
    ENEMYDELAY = 3
    ENEMYSPEED = 300
    generateAsteroid(universe.width/2-2800, universe.height/2-500)
    generateAsteroid(universe.width/2-2200, universe.height/2-600)
    generateAsteroid(universe.width/2-1600, universe.height/2-500)
    generateAsteroid(universe.width/2-1000, universe.height/2-600)
    generateAsteroid(universe.width/2+1000, universe.height/2-500)
    generateAsteroid(universe.width/2+1600, universe.height/2-600)
    generateAsteroid(universe.width/2+2200, universe.height/2-500)
    generateAsteroid(universe.width/2+2800, universe.height/2-600)
  elseif level == 3 then
    self.turretsLeft = 30
    ENEMYDELAY = 1
    ENEMYSPEED = 400
  end
end

function initiateMainmenu()
  mainMenu = Menu:new({
		x = love.graphics.getWidth()/6,
		y = love.graphics.getHeight()/4 + 200,
    type = "main"})

  table.insert(mainMenu.ItemsNorm, menuGif1_1_norm)
  table.insert(mainMenu.ItemsHigh, menuGif1_1_high)
  table.insert(mainMenu.ItemsNorm, menuGif1_2_norm)
  table.insert(mainMenu.ItemsHigh, menuGif1_2_high)
  table.insert(mainMenu.ItemsNorm, menuGif1_3_norm)
  table.insert(mainMenu.ItemsHigh, menuGif1_3_high)

  mainMenu.initiated = true
end

function initiateShop()
  shop = Menu:new({
		x = 0,
		y = 0,
    type = "shop"})

  table.insert(shop.ItemsHigh, Shopbutton:new({x = 940, y = 132, sprite = shopArrowLeftHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1584, y = 132, sprite = shopArrowRightHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 940, y = 229, sprite = shopArrowLeftHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1584, y = 229, sprite = shopArrowRightHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1205, y = 334, sprite = shopBuyHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094, y = 470, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129, y = 470, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129, y = 470, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129+129, y = 470, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129+129+129, y = 470, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094, y = 470+129, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129, y = 470+129, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129, y = 470+129, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129+129, y = 470+129, sprite = shopInvHighlite}))
  table.insert(shop.ItemsHigh, Shopbutton:new({x = 1094+129+129+129+129, y = 470+129, sprite = shopInvHighlite}))

  shop.initiated = true
end
