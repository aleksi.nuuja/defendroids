Audio = {}

function Audio:new(params)
  o = {}
  o.initiated = false
  o.expRot = 1 -- eplosions rotate sound sources 1, 2 and 3
  o.expDelay = 0.6 -- so many explosions there has to be a small delay in between
  o.turRot = 13 -- turret gun sounds rotate sound sources 13, 14 and 15
  o.railRot = 24 -- railgun sounds rotate sound sources 24, 25 and 26
  o.SoundFx = {}
  o.Music = {}
  o.fadingInMusic = false

  setmetatable(o, self)
  self.__index = self
  return o
end

-- not needed at all?
function Audio:draw()

end

function Audio:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateAudio()
  end
  self.expDelay = self.expDelay - 1*dt
end

function Audio:initiateAudio()
  self.SoundFx[1] = love.audio.newSource("sounds/explo5.wav", "static")
  self.SoundFx[2] = love.audio.newSource("sounds/explo5.wav", "static")
  self.SoundFx[3] = love.audio.newSource("sounds/explo5.wav", "static")
  self.SoundFx[4] = love.audio.newSource("sounds/edited/rapid1.wav", "static")
  self.SoundFx[5] = love.audio.newSource("sounds/edited/rapid2.wav", "static")
  self.SoundFx[6] = love.audio.newSource("sounds/edited/rapid3.wav", "static")
  self.SoundFx[7] = love.audio.newSource("sounds/rapid_new1.wav", "static")
  self.SoundFx[8] = love.audio.newSource("sounds/rapid_new1.wav", "static")
  self.SoundFx[9] = love.audio.newSource("sounds/rapid_new1.wav", "static")
  self.SoundFx[10] = love.audio.newSource("sounds/rapid_new1.wav", "static")
  self.SoundFx[11] = love.audio.newSource("sounds/edited/rapid8.wav", "static")
  self.SoundFx[12] = love.audio.newSource("sounds/heavygun.wav", "static")
  self.SoundFx[13] = love.audio.newSource("sounds/rapidnew.wav", "static")
  self.SoundFx[14] = love.audio.newSource("sounds/rapidnew.wav", "static")
  self.SoundFx[15] = love.audio.newSource("sounds/rapidnew.wav", "static")
  self.SoundFx[16] = love.audio.newSource("sounds/explo5.wav", "static")
  self.SoundFx[17] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[18] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[19] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[20] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[21] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[22] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[23] = love.audio.newSource("sounds/bomb_short.wav", "static")
  self.SoundFx[24] = love.audio.newSource("sounds/railgun1.wav", "static")
  self.SoundFx[25] = love.audio.newSource("sounds/railgun1.wav", "static")
  self.SoundFx[26] = love.audio.newSource("sounds/railgun1.wav", "static")
  self.SoundFx[27] = love.audio.newSource("sounds/missile.wav", "static")

  self.Music[1] = love.audio.newSource("sounds/slave_unit_-_evo.mp3")
  self.Music[2] = love.audio.newSource("sounds/defendroids_combat_01_raw.mp3")
end
