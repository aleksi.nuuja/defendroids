function checkCollisions(s)
  -- bullets hitting enemies
  for i, enemy in ipairs(enemies) do
    for j, bullet in ipairs(bullets) do
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, bullet.x, bullet.y, 10, 10) and enemies[i].alive then
        table.remove(bullets, j)
        enemies[i].alive = false
      end
    end

    -- enemies hitting turrets (remove both, add explosion)
    for j, turret in ipairs(turrets) do
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, turret.x-20, turret.y-20, 40, 40) and enemies[i].alive then
        table.remove(turrets, j)
        enemies[i].alive = false
        textLogger:newMessage("You lost a turret!", "red")
        createFlashtext(turret.x, turret.y, "YOU LOST A TURRET", "red", "combat")
        -- explosion!
        table.insert(explosions, Explosion:new({
          x = turret.x,
          y = turret.y,
          size = 6,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end

    -- enemies hitting mines
    for j, mine in ipairs(mines) do
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, mine.x-20, mine.y-20, 40, 40) and enemies[i].alive then
        enemies[i].alive = false
        table.remove(mines, j)
        -- explosion!
        local explosize = 6
        if mine.type == "projectile" then explosize = 2 end
        table.insert(explosions, Explosion:new({
          x = mine.x,
          y = mine.y,
          size = explosize,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end

    -- enemies hitting rockets
    for j, rocket in ipairs(rockets) do
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, rocket.x-20, rocket.y-20, 40, 40) and enemies[i].alive then
        enemies[i].alive = false
        table.remove(rockets, j)
        -- explosion!
        local explosize = 3
        if rocket.type == "large" then explosize = 6 end
        table.insert(explosions, Explosion:new({
          x = rocket.x,
          y = rocket.y,
          size = explosize,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end


    -- enemies hitting explosion bounding boxes (being destroyed by blasts)
    for j, exp in ipairs(explosions) do
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, exp.boundingBoxX, exp.boundingBoxY, exp.boundingBoxWidth, exp.boundingBoxHeight) and enemies[i].alive then
        enemies[i].alive = false
        exp.killcounter = exp.killcounter + 1
        -- increase counter how many enemies this explosion destroyed - display this in a flashtext "524 kills"
      end
    end

    -- enemies hitting player
      if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, ship.x-20, ship.y-20, 40, 40) and enemies[i].alive and ship.alive and not(ship.indestructable) then
      enemies[i].alive = false
      if not(ship.hurt) then -- first collision gets you hurt and you start emitting smoke
        ship.hurt = true
        ship.indestructable = true
      else
        ship.alive = false
        breach = true -- this is game over
      end
    end

    -- enemies hitting asteroids
    for j, asteroid in ipairs(asteroids) do
      if CheckCollision(enemy.x, enemy.y, 40, 40, asteroid.x, asteroid.y, asteroid.width, asteroid.height) and enemies[i].alive then
        asteroid.xspeed = asteroid.xspeed + enemy.xspeed / 100000
        asteroid.yspeed = asteroid.yspeed + enemy.yspeed / 100000
        if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 1 end
        if asteroid.hitpoints <= 0 then table.remove(asteroids, j) end
        enemies[i].alive = false
        if asteroid.type == "heavy" then
          -- find out on which path segment the collision occurs and increase the corresponding counter
          -- this is the index of the coordinate on the Paths[] list, which is a list of x,y coordinates, dividing it by 2 gets the segment number
          local collidingSegmentStartY = swarmPathsHandler:getPreviousPoint(enemy.y, enemy.pathId)
          local collidingSegment = collidingSegmentStartY/2

          swarmPathsHandler.collisionCounter[enemies[i].pathId][collidingSegment] = swarmPathsHandler.collisionCounter[enemies[i].pathId][collidingSegment] + 1
          swarmPathsHandler.collisionY[enemies[i].pathId] = enemy.y -- set the collisionY on this segment to the Y of last collision occurred
        end
      end
    end

    -- enemies hitting bottom wall -- this is a breach and means game over!
    if enemy.y > universe.height and enemy.alive then
      gameOver("breach") -- defined in maingame.lua
      if cameraOnShip then tweenEngine:createTween("cameraY", ship.y, universe.height, 2, easeInOutCubic) end
      cameraOnShip = false -- moves camera location to bottom of universe
      table.remove(enemies, i)
    end
  end -- end of enemy collisions for loop

  -- asteroids hitting turrets (when pushed on top)
  for i, asteroid in ipairs(asteroids) do
    for j, turret in ipairs(turrets) do
      if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, turret.x-40, turret.y-40, 80, 80) then
        table.remove(turrets, j)
        textLogger:newMessage("You lost a turret!", "red")
        createFlashtext(turret.x, turret.y, "YOU LOST A TURRET", "red", "combat")
        -- explosion!
        table.insert(explosions, Explosion:new({
          x = turret.x,
          y = turret.y,
          size = 4,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end
  end

  -- bullets hitting asteroids
  for i, asteroid in ipairs(asteroids) do
    for j, bullet in ipairs(bullets) do
      if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, bullet.x-5, bullet.y-5, 10, 10) then
        table.remove(bullets, j)
        asteroid.xspeed = asteroid.xspeed + bullet.xspeed / 100000
        asteroid.yspeed = asteroid.yspeed + bullet.yspeed / 100000
        if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 1 end
        if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
      end
    end
  end

  -- bullets hitting "normal" type mines - they move a little
  for i, mine in ipairs(mines) do
    if mine.type == "normal" then
      for j, bullet in ipairs(bullets) do
        if CheckCollision(mine.x-50, mine.y-50, 100, 100, bullet.x-5, bullet.y-5, 10, 10) then
          table.remove(bullets, j)
          mine.xspeed = mine.xspeed + bullet.xspeed / 25000
          mine.yspeed = mine.yspeed + bullet.yspeed / 25000
        end
      end
    end
  end

  --   bullets hitting turrets: they move a little
	for i, turret in ipairs(turrets) do
		for j, bullet in ipairs(bullets) do
			if CheckCollision(turret.x-40, turret.y-40, 80, 80, bullet.x-5, bullet.y-5, 10, 10) then
        turret.xspeed = turret.xspeed + bullet.xspeed / 25000
        turret.yspeed = turret.yspeed + bullet.yspeed / 25000
			  table.remove(bullets, j)
			end
		end
	end

  -- rockets hitting asteroids
  for i, asteroid in ipairs(asteroids) do
    for j, rocket in ipairs(rockets) do
      if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, rocket.x-5, rocket.y-5, 10, 10) then
        table.remove(rockets, j)
        asteroid.xspeed = asteroid.xspeed + rocket.xspeed / 5000
        asteroid.yspeed = asteroid.yspeed + rocket.yspeed / 5000
        if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 10 end
        if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
        -- explosion!
        local explosize = 3
        if rocket.type == "large" then explosize = 6 end
        table.insert(explosions, Explosion:new({
          x = rocket.x,
          y = rocket.y,
          size = explosize,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end
  end

  -- mines hitting asteroids
  for i, asteroid in ipairs(asteroids) do
    for j, mine in ipairs(mines) do
      if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, mine.x-50, mine.y-50, 100, 100) then
        table.remove(mines, j)
        asteroid.xspeed = asteroid.xspeed + mine.xspeed / 500
        asteroid.yspeed = asteroid.yspeed + mine.yspeed / 500
        if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 10 end
        if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
        -- explosion!
        local explosize = 8
        if mine.type == "projectile" then explosize = 2 end
        table.insert(explosions, Explosion:new({
          x = mine.x,
          y = mine.y,
          size = explosize,
          angle = math.random()*math.pi*2}))
        audioExplosion()
      end
    end
  end

  -- player hitting asteroids
  local deadmanswitch = 0
  for i = 1, #asteroids do
    if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ship.x-70, ship.y-70, 140, 140) then
      local slowdown
  --				if asteroids[i].type == "heavy" then slowdown = 0.6 else slowdown = 0.8 end
      asteroids[i].xspeed = ship.xspeed --* slowdown
      asteroids[i].yspeed = ship.yspeed --* slowdown
      ship.xspeed = ship.xspeed --* slowdown
      ship.yspeed = ship.yspeed --* slowdown
      repeat
        deadmanswitch = deadmanswitch + 1
        asteroids[i].x = asteroids[i].x + ship.xspeed
        asteroids[i].y = asteroids[i].y + ship.yspeed
      until deadmanswitch > 1000 or not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ship.x-70, ship.y-70, 140, 140))
      if deadmanswitch > 1000 then print("player hitting asteroid repeat loop just used dead man's switch of over 1000 repeats") end
    end
  end

  -- asteroids hitting asteroids
  local count = 1
  for i = 1, #asteroids-1 do
    for j = 1 + count, #asteroids do
        if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height) then

          -- calculate speed for both asteroids, adapt the faster moving asteroid's speed for both asteroids
          local iSpeed = math.sqrt(asteroids[i].xspeed * asteroids[i].xspeed + asteroids[i].yspeed * asteroids[i].yspeed)
          local jSpeed = math.sqrt(asteroids[j].xspeed * asteroids[j].xspeed + asteroids[j].yspeed * asteroids[j].yspeed)

          if iSpeed > jSpeed then
            asteroids[j].xspeed = asteroids[i].xspeed
            asteroids[j].yspeed = asteroids[i].yspeed
          else
            asteroids[i].xspeed = asteroids[j].xspeed
            asteroids[i].yspeed = asteroids[j].yspeed
          end

          -- try and move the first one a bit and test if they still overlap, if yes, cancel the move and move the other one bit and test if they overlap
          -- if still yes, increase the step and try again until you find a step that frees the willies!!!
          local clearedOverlap = false
          local step = 1
          repeat
            asteroids[i].x = asteroids[i].x + asteroids[i].xspeed*step
            asteroids[i].y = asteroids[i].y + asteroids[i].yspeed*step
            if not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height)) then clearedOverlap = true end
            if not clearedOverlap then
              asteroids[i].x = asteroids[i].x - asteroids[i].xspeed*step
              asteroids[i].y = asteroids[i].y - asteroids[i].yspeed*step
              asteroids[j].x = asteroids[j].x + asteroids[j].xspeed*step
              asteroids[j].y = asteroids[j].y + asteroids[j].yspeed*step
              if not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height)) then clearedOverlap = true end
              if not clearedOverlap then
                asteroids[j].x = asteroids[j].x - asteroids[j].xspeed*step
                asteroids[j].y = asteroids[j].y - asteroids[j].yspeed*step
              end
            end
            step = step + 1

          until clearedOverlap or step > 10

        end
    end
    count = count + 1 -- for each round there are less to check - to find all possible pairs
  end
end

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end
