Tween = {}

function Tween:new(params)
  o = {}

  o.Keys = params.Keys
 	o.Values = params.Values
 	o.Tweens = params.Tweens

	-- we put values we want to be able to tween around in a table with keys and values
 	o.Tweenables = getInitValues()

  -- we split the table to two arrays, one with all the keys and one with all values
	-- now we can access the value by using the index of the key
	for i = 1, #o.Tweenables do
		if not (i % 2 == 0) then -- if odd
			table.insert(o.Keys, o.Tweenables[i])
    else
			table.insert(o.Values, o.Tweenables[i])
    end
  end

  setmetatable(o, self)
  self.__index = self
  return o
end

function Tween:debug()
  print(#self.Keys)
end

function getInitValues()
  return {"scale", 1, "textSize", 5, "textY", love.graphics.getHeight()-80, "alpha", 255, "logTextY", 0, "logAlpha", 0, "WCTextX", 0, "WCAlpha", 0, "musicVolume", 0, "cameraY", 0, "creditsMeter", 0}
end

function Tween:update(dt)
  -- update all tweens
	local i, o, i2
	for i, o in ipairs(self.Tweens) do
		for i2 = 1, #self.Keys do
			if self.Keys[i2] == self.Tweens[i].key then -- if there is a Tween created for this key, tween the corresponding value
				self.Tweens[i].val = self.Tweens[i].val + 1*dt*self.Tweens[i].speed -- val goes from 0 to 100
				if self.Values[i2] < self.Tweens[i].target then self.Values[i2] = self.Values[i2] + self.Tweens[i].increment*dt*self.Tweens[i].speed end
				if self.Values[i2] > self.Tweens[i].target then self.Values[i2] = self.Values[i2] - self.Tweens[i].increment*dt*self.Tweens[i].speed end
				if self.Tweens[i].val >= 100 then -- tween is complete at val 100
          self.Values[i2] = self.Tweens[i].target -- values are approximately right so set to actual target value at the end
					table.remove(self.Tweens, i)
          break
				end
			end
		end
	end
end

function Tween:returnValue(keyName)
  for i = 1, #self.Keys do
    if self.Keys[i] == keyName then return self.Values[i] end
  end
  return "NO VALUE FOUND IN TWEENABLES LIST"
end

function Tween:setValue(keyName, newValue)
  for i = 1, #self.Keys do
    if self.Keys[i] == keyName then self.Values[i] = newValue end
  end
end

-- speed is a multiplier, suggest using values 1 to 200 (slow to fast tween)
function Tween:createTween(keyName, startValue, targetValue, speed)
  -- check if the value is already being tweened - multiples not allowed!
  local alreadyTweening = false
  for i = 1, #self.Tweens do
    if self.Tweens[i].key == keyName then
--      print("ALREADY TWEENING! keyName = " .. keyName)
      alreadyTweening = true
    end
  end

  if not alreadyTweening then
    self:setValue(keyName, startValue)
    local delta = math.abs(startValue - targetValue) -- calculate the tween distance: difference of start and end values
    delta = delta / 100 -- split to 100 equal steps

    table.insert(self.Tweens, {
      key = keyName,
      start = startValue,
      target = targetValue,
      increment = delta,
      speed = speed,
      val = 0}) -- val goes from 0 to 100
  end
end
