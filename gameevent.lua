Gameevent = {}

function Gameevent:new(params)
  o = {}
  o.triggered = false
  o.trigger = params.trigger
  o.payload = params.payload

  setmetatable(o, self)
  self.__index = self
  return o
end

function Gameevent:update(dt)
  local checkTrigger = self.trigger()
  if checkTrigger and not (self.triggered) then
    self.triggered = true
    self.payload() 
  end
end
