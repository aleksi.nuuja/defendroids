Enemy = {}

function Enemy:new(params)
  o = {}
  o.speed = params.speed
  o.angle = 0 -- direction as radians (2*pi is 360 degrees), zero is towards right
  o.x = params.x
  o.y = 0
  o.targetY = 0
  o.pathId = params.pathId
  o.spread = params.spread

  o.xspeed = 0
  o.yspeed = params.speed
  o.isMark = false
  o.alive = true
  o.isVisible = true
  o.proximityAlerted = false
  o.explosionState = 1

  o.sprite = love.graphics.newImage("graphics/hand_enemy1.png")

  setmetatable(o, self)
  self.__index = self
  return o
end

function Enemy:update(dt)
	self.x = self.x + self.xspeed * dt
  self.y = self.y + self.yspeed * dt

  -- check if any segment in my path is halted, if yes, set hover Y coordinate to the next halt Y point below me
  local i
  for i=1,11 do
    if swarmPathsHandler.segmentHalted[self.pathId][i] then
      if self.y < swarmPathsHandler.haltY[self.pathId][i] then
        self.targetY = swarmPathsHandler.haltY[self.pathId][i] - 500
        break
      end
    end
    self.targetY = 0
  end

  if self.targetY > 0 then -- if path is halted hover around haltY coordinate
    -- accelerate towards targetY - in proportion to distance, with some random variance
    local deltaToPathY = self.targetY - self.y
    self.yspeed = self.yspeed + deltaToPathY/20*(1+math.random()*6)
    if self.yspeed < -1 * self.speed then self.yspeed = -1 * self.speed end -- limit hovering speed
    if self.yspeed > self.speed then self.yspeed = self.speed end
  else
    self.yspeed = self.speed
  end

  self.angle = math.atan2(self.yspeed, self.xspeed)

  -- universe boundaries
  if self.x < 40 then self.x = 40  end
  if self.x > universe.width-40 then self.x = universe.width-40  end
  if self.y < 0 then -- bounce back
    self.y = 0
    self.yspeed = -1 * self.yspeed
  end

  -- optimizinggg check if the ship is within visible camera zoom (do not render sprite if not)
  self.isVisible = self:checkIfVisible()

  -- steer towards path logic:
  local previousPoint
  local nextPoint
  previousPoint = swarmPathsHandler:getPreviousPoint(self.y, self.pathId)
  nextPoint = swarmPathsHandler:getNextPoint(self.y, self.pathId)
  local targetX = self:findTargetX(self.y, self.pathId, previousPoint, nextPoint)

  -- accelerate towards path - in proportion to distance, with some random variance
  local deltaToPathX = targetX - self.x
  self.xspeed = self.xspeed + deltaToPathX/10*(1+math.random()*6)
  if self.xspeed > self.spread then self.xspeed = self.spread end -- limit max speed towards path
  if self.xspeed < -self.spread then self.xspeed = -self.spread end -- limit max speed towards path

  -- alert player if close to breach
  if self.y > universe.height - 5000 and self.proximityAlerted == false then
    self.proximityAlerted = true
    textLogger:newMessage("Alert! Enemy closing in on base", "red")
  end

  if not (self.alive) then self.explosionState = self.explosionState + 1 end

end

function Enemy:findTargetX(y, pathId, prevPointId, nextPointId)
  -- this is mathematics
  -- solve where the path between prevpoint and nextpoint intersects with the line y
  -- --> to find offset between current x coordinate and the path's x coordinate what we are supposed to follow
  if y > universe.height then -- these are out of universe bounds and can be given any value
    return 0
  end

  -- find prev point x and y
  local prevPointX = swarmPathsHandler.Paths[pathId][prevPointId-1] -- the ID points to Y value, X is previous in the list
  local prevPointY = swarmPathsHandler.Paths[pathId][prevPointId]
  -- find next point x and y
  local nextPointX = swarmPathsHandler.Paths[pathId][nextPointId-1] -- the ID points to Y value, X is previous in the list
  local nextPointY = swarmPathsHandler.Paths[pathId][nextPointId]

  if prevPointX == nil then print("Nil tuli swarmPathsHandler.Paths[" .. pathId .. "][".. prevPointId-1 .. "]") end
  if prevPointY == nil then print("Nil tuli swarmPathsHandler.Paths[" .. pathId .. "][".. prevPointId .. "]") end
  if nextPointX == nil then print("Nil tuli swarmPathsHandler.Paths[" .. pathId .. "][".. nextPointId-1 .. "]") end
  if nextPointY == nil then print("Nil tuli swarmPathsHandler.Paths[" .. pathId .. "][".. nextPointId .. "]") end

  -- calculate slope of the line between these points:
  -- calculate delta x (positive or negative matters here!)
  -- calculate delta y (should always be positive)
  local deltaX = nextPointX - prevPointX
  local deltaY = nextPointY - prevPointY
  -- slope is delta x / delta y (how much X delta there is for this amount of y delta)
  local slope = deltaX / deltaY
  -- then find out where this intersects with enemy's current y:
  -- calculate delta y from previous point
  local delta2Y = y - prevPointY
  -- targetX is previous point X + delta y * slope
  return prevPointX + (delta2Y*slope)
end

function Enemy:checkIfVisible()
  local s = gameStates.maingame
	local screenheight = (love.graphics.getHeight() / tv("scale"))
	local midpointy = - scrolloffsetY + (screenheight / 2)
  local lowerYBoundary = midpointy - screenheight/2 - 40 -- hardcode 40 pix buffer above and below visible screen so that enemies are drawn when closing the boundary
  local upperYBoundary = midpointy + screenheight/2 + 40
  if self.y > lowerYBoundary and self.y < upperYBoundary then return true else return false end
end

function Enemy:draw(x, y, angle)
  if self.alive then
    if self.isVisible then
      love.graphics.setColor(255, 255, 255)
      love.graphics.draw(self.sprite, x, y, angle-math.pi/2, 1, 1, 70, 93)
    end
  else
    if self.isVisible then
      if self.explosionState < 12 then
        love.graphics.setColor(255, 255, 255)
        love.graphics.draw(exp[self.explosionState], x+150, y-30, 2, 2, 2)
      end
    end
  end
end
