SwarmPath = {}

-- this is the swarmpathhandler, handling a list of Paths
-- each path is a list consisting of x,y coordinates of 11 sequential points
function SwarmPath:new(params)
  o = {}
  o.initiated = false
  o.Paths = {}
  o.collisionCounter = {} -- each path has a counter for collisions FOR EACH SEGMENT - a lot of collisions means the path needs to adapt
  o.collidingSegmentStartY = 0 -- when collisions occur, the Y coordinate of the previous path point is tracked (this could be local variable in main)
  o.collisionY = {} -- the actual y coordinate of the collision is used for halting swarm
  o.evasionDirection = {} -- each path has a evasion direction for finding a way around obstacles FOR EACH SEGMENT (left or right)

  o.brightness = 100 -- used for pulsating arrows displayed on attack path when player is planning defense
  o.brightnessModifier = 2

  o.segmentHalted = {} -- for each path, a list of 11 boolean values if any of the segments is blocked and needs to adapt
  o.haltY = {} -- a list of 11 y-coordinates for each path, the halting Y is calculated from the actual collisionY, a bit above the collision
  o.searchPathDelay = 5 -- increase this to make swarm slower to find new path

  setmetatable(o, self)
  self.__index = self
  return o
end

function SwarmPath:draw()
  s = gameStates.maingame
  local i
  love.graphics.setColor(0,255,0)

  if not(drawPathsDebug) then
    if isStartGame then
      for i=1,#self.Paths-1 do -- last path is always hidden (testing the idea here)
  --      love.graphics.line(self.Paths[i]) -- debugging draw, remove this
        self:drawPathIndicators(i)
      end
--    else -- let's test this: draw the segment that enemy is retargetting as red thin line
--      for i=1,#self.Paths do --
--        self:drawHaltedSegments(i)
--      end
    end
  else
    for i=1,#self.Paths do
      love.graphics.line(self.Paths[i]) -- debugging draw, remove this
    end
  end
end

function SwarmPath:drawPathIndicators(i)
  for i2=1, #self.Paths[i]-3, 2 do -- traverse through path in steps of 2, stop at one before last
    self:drawArrow(self.Paths[i][i2], self.Paths[i][i2+1], self.Paths[i][i2+2], self.Paths[i][i2+3])
  end
end

function SwarmPath:drawHaltedSegments(i)
  local segment
  for segment=1,11 do -- traverse through 11 segments
    if self.segmentHalted[i][segment] then
      local segStartX = segment*2-1 -- index of the X coordinate of the segment starting point
      local segStartY = segment*2 -- index of the Y coordinate of the segment starting point
      local segEndX = (segment+1)*2-1 -- index of the X coordinate of the segment ending point
      local segEndY = (segment+1)*2 -- index of the Y coordinate of the segment ending point

      love.graphics.setColor(255,0,200)
      love.graphics.getLineWidth(1)
      love.graphics.line(self.Paths[i][segStartX], self.Paths[i][segStartY], self.Paths[i][segEndX], self.Paths[i][segEndY]) -- debugging draw, remove this
    end
  end
end


function SwarmPath:drawArrow(x1, y1, x2, y2)
  local angle = - math.atan2(x2 - x1, y2 - y1) + math.pi/2 -- calculate angle of the vector from 1 to 2
  local length = math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))

  -- arrow starting and ending point are along the original line, but shorter
  local ax1 = x1 + math.cos(angle)*(length/2-200)
  local ay1 = y1 + math.sin(angle)*(length/2-200)
  local ax2 = x2 - math.cos(angle)*(length/2-200)
  local ay2 = y2 - math.sin(angle)*(length/2-200)

  local arrowHeadAngle = math.pi * 4/5

  local sidepointleftX = ax2 + math.cos(angle+(arrowHeadAngle))*100
  local sidepointleftY = ay2 + math.sin(angle+(arrowHeadAngle))*100
  local sidepointrightX = ax2 + math.cos(angle-(arrowHeadAngle))*100
  local sidepointrightY = ay2 + math.sin(angle-(arrowHeadAngle))*100

  love.graphics.setColor(0, self.brightness, 0) -- pulsating colour
  love.graphics.setLineWidth(10)

  love.graphics.line(ax1, ay1, ax2, ay2)
  love.graphics.line(ax2, ay2, sidepointleftX, sidepointleftY)
  love.graphics.line(ax2, ay2, sidepointrightX, sidepointrightY)
end

function SwarmPath:update(dt)
  s = gameStates.maingame

  if not self.initiated then
    self.initiated = true
    self:initiateSwarmPath()
  end

  -- check if any segment on any path is producing a lot of collisions - if yes, it needs to adapt!
  local i, j
  for i=1,#self.Paths do
    for j=1,11 do
      if self.collisionCounter[i][j] > 100 then -- was > 5
        self.collisionCounter[i][j] = 0
        -- bugfix: only one segment at a time can be halted
        -- so when setting one segment halted we need to clear all other halt flags
        self:clearAllHaltedSegments(i)
        self.segmentHalted[i][j] = true
        self.haltY[i][j] = self.collisionY[i]
        textLogger:newMessage("Enemy is scanning for clear path.", "red")
        --      self:findNewPath(i)
      end

      if self.segmentHalted[i][j] then
        local clearPathFound = false
        self.searchPathDelay = self.searchPathDelay - 1
        if self.searchPathDelay == 0 then
          self.searchPathDelay = 5
          clearPathFound = self:findNewPath(i,j)
          if clearPathFound then
            textLogger:newMessage("Enemy has found a new clear path.", "red")
            self.segmentHalted[i][j] = false
          end
        end
      end
    end
  end

  -- pulsating brightness for path arrows
  self.brightness = self.brightness + self.brightnessModifier
  if self.brightness < 100 or self.brightness > 254 then self.brightnessModifier = self.brightnessModifier * -1 end
end

function SwarmPath:clearAllHaltedSegments(pathId)
  local i
  for i=1,11 do
    self.segmentHalted[pathId][i] = false
  end
end

function SwarmPath:initiateSwarmPath()

  -- this should be a loop from 1 to 11 where you always add or subtract a step from the previous x coordinate
  -- and use table.insert to add a new point to the path
  local i
  for i=1,4 do
    self.Paths[i] = randomizePath()
  end

  local i, j, val

  for i=1,#self.Paths do
    self.collisionCounter[i] = {}
    self.evasionDirection[i] = {}
    self.segmentHalted[i] = {}
    self.haltY[i] = {}

    for j=1,11 do
      self.collisionCounter[i][j] = 0 -- each path segment has a collision counter, initiate them to zero
      self.segmentHalted[i][j] = false -- each path segment has a boolean stating if they are blocked and need to halt enemies
      self.haltY[i][j] = 0 -- each path segment has a coordinate value where the enemies should hover while looking for clear path
      val = math.random(2)*2-3 -- randomize evasionDirection -1 or +1
      table.insert(self.evasionDirection[i], val)
    end
    self.collisionY[i] = 0
  end
end

function randomizePath()
  local pathToReturn = {}
  local stepsize = universe.width/8
  local point = math.random(8)
  local i, step

  for i=1,11 do
    if point == 8 or point == 7 then step = math.random(2)-3
    elseif point == 0 or point == 1 then step = math.random(2)
    else step = math.random(5)-3 end -- values -2, -1, 0, 1, 2
    point = point + step
    table.insert(pathToReturn, point*stepsize) -- x coordinate
    if i == 1 then
      table.insert(pathToReturn, -20)
    elseif i == 11 then
      table.insert(pathToReturn, universe.height+100)
    else
      table.insert(pathToReturn, (i-1)/10 * universe.height)
    end
  end
  return pathToReturn
end

function SwarmPath:findNewPath(pathId, segment) -- adjusts path points on colliding segment
  -- when a collision occurs on a segment the segment is halted - all enemies on that path hover around the next collision coordinate below them
  -- the path segment moves a little and run test afterwards is the segment is clear --> if yes, the halt is removed

  local segStartX = segment*2-1 -- index of the X coordinate of the segment starting point
  local segStartY = segment*2 -- index of the Y coordinate of the segment starting point
  local segEndX = (segment+1)*2-1 -- index of the X coordinate of the segment ending point
  local segEndY = (segment+1)*2 -- index of the Y coordinate of the segment ending point

  -- evasionDirection table has pre-randomized adjustment to left or right
  local adjustment = self.evasionDirection[pathId][segment] * 10

  -- if the path is at edge of universe (both X coordinates, segment start and end), the evasion direction is reversed and speed doubled
  if (self.Paths[pathId][segStartX] <= 0 and self.Paths[pathId][segEndX] <= 0) then self.evasionDirection[pathId][segment] = 2 end
  if (self.Paths[pathId][segStartX] >= universe.width and self.Paths[pathId][segEndX] >= universe.width) then self.evasionDirection[pathId][segment] = -2 end

  -- adjustion is doubled for top or bottom coordinate of the line segment so that it tends to become more steeply vertical
  if adjustment > 0 then
    if self.Paths[pathId][segStartX] >= self.Paths[pathId][segEndX] then
      self.Paths[pathId][segStartX] = self.Paths[pathId][segStartX] + adjustment/4
      self.Paths[pathId][segEndX] = self.Paths[pathId][segEndX] + adjustment
    else
      self.Paths[pathId][segStartX] = self.Paths[pathId][segStartX] + adjustment
      self.Paths[pathId][segEndX] = self.Paths[pathId][segEndX] + adjustment/4
    end
  else
    if self.Paths[pathId][segStartX] >= self.Paths[pathId][segEndX] then
      self.Paths[pathId][segStartX] = self.Paths[pathId][segStartX] + adjustment
      self.Paths[pathId][segEndX] = self.Paths[pathId][segEndX] + adjustment/4
    else
      self.Paths[pathId][segStartX] = self.Paths[pathId][segStartX] + adjustment/4
      self.Paths[pathId][segEndX] = self.Paths[pathId][segEndX] + adjustment
    end
  end

  -- segment end point X coordinates never go past universe boundaries
  if self.Paths[pathId][segStartX] < 0 then self.Paths[pathId][segStartX] = 0 end
  if self.Paths[pathId][segStartX] > universe.width then self.Paths[pathId][segStartX] = universe.width end
  if self.Paths[pathId][segEndX] < 0 then self.Paths[pathId][segEndX] = 0 end
  if self.Paths[pathId][segEndX] > universe.width then self.Paths[pathId][segEndX] = universe.width end

  -- TEST IF PATH IS NOW CLEAR OR NOT and return false/true accordingly
  local x1 = self.Paths[pathId][segStartX]
  local y1 = self.Paths[pathId][segStartY]
  local x2 = self.Paths[pathId][segEndX]
  local y2 = self.Paths[pathId][segEndY]
  if not(isSegmentClear(x1,y1,x2,y2,300)) then return false else return true end

end

function isSegmentClear(x1, y1, x2, y2, marginal)
  s = gameStates.maingame
  for j, asteroid in ipairs(asteroids) do
    if lineIntersects(asteroid.x-marginal, asteroid.y-marginal, asteroid.x+asteroid.width+marginal, asteroid.y-marginal, x1, y1, x2, y2) then return false end
    if lineIntersects(asteroid.x-marginal, asteroid.y-marginal, asteroid.x-marginal, asteroid.y+asteroid.height+marginal, x1, y1, x2, y2) then return false end
    if lineIntersects(asteroid.x-marginal, asteroid.y+asteroid.height+marginal, asteroid.x+asteroid.width+marginal, asteroid.y+asteroid.height+marginal, x1, y1, x2, y2) then return false end
    if lineIntersects(asteroid.x+asteroid.width+marginal, asteroid.y-marginal, asteroid.x+asteroid.width+marginal, asteroid.y+asteroid.height+marginal, x1, y1, x2, y2) then return false end
  end

  return true
end

-- Adapted from: http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/1968345#1968345
--
function lineIntersects(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y)
  local s1_x = p1_x - p0_x
  local s1_y = p1_y - p0_y
  local s2_x = p3_x - p2_x
  local s2_y = p3_y - p2_y

  local s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y)
  local t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y)

  if (s >= 0 and s <= 1 and t >= 0 and t <= 1) then
      -- Collision detected
      return true
  end

  return false -- No collision
end

-- some return values seem to be nil that causes crash!!!
function SwarmPath:getPreviousPoint(y, pathId) -- returns index of the Y value
  local i = 2
  -- traverse every other value in Path, only Y coordinates
  for i=2, #self.Paths[pathId], 2 do
    if self.Paths[pathId][i] >= y then
--      print("previous point is " .. i-2)
      return i-2
    end
	end
end

-- some return values seem to be nil that causes crash!!!
function SwarmPath:getNextPoint(y, pathId) -- returns index of the Y value
  local i = 2
  -- traverse every other value in Path, only Y coordinates
  for i=2, #self.Paths[pathId], 2 do
    if self.Paths[pathId][i] >= y then
--      print("next point is " .. i)
      return i
    end
	end
end
