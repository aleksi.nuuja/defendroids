Weaponconsole = {}

function Weaponconsole:new(params)
  o = {}
  o.Icons = {}
  o.weaponOfChoice = "RFCI"
  o.previousWeapon = ""
  o.initiated = false
  o.animNow = false

  local font = love.graphics.newFont("graphics/Krungthep.ttf", 12)
  o.msg = love.graphics.newText(font, "")

  o.x = params.x
  o.y = params.y

  o.stencil = function () end

  setmetatable(o, self)
  self.__index = self
  return o
end

function Weaponconsole:draw()
  local w_id = inventory:returnIndex(self.weaponOfChoice)
  local prev_w_id = inventory:returnIndex(self.previousWeapon)

  if self.initiated then
      love.graphics.setColor(0, 0, 0)
      love.graphics.circle("fill", self.x+50, self.y+50, 80)

      -- draw a circle as a stencil. Each pixel touched by the circle will have its stencil value set to 1. The rest will be 0.
      love.graphics.stencil(stencil, "replace", 1)

      -- Only allow rendering on pixels which have a stencil value greater than 0.
      love.graphics.setStencilTest("greater", 0)

      if not(self.weaponOfChoice == "") then
        if self.animNow then
          love.graphics.setColor(255, 255, 255, tv("WCAlpha")) -- alpha tweens from 255 to 0
          love.graphics.draw(inventory.Items[prev_w_id].icon, self.x - tv("WCTextX"), self.y)
          love.graphics.setColor(255, 255, 255, 255-tv("WCAlpha"))
          love.graphics.draw(inventory.Items[w_id].icon, self.x - tv("WCTextX") + 100, self.y)
        else
          love.graphics.setColor(255, 255, 255)
          love.graphics.draw(inventory.Items[w_id].icon, self.x, self.y)
        end
      end

      love.graphics.setStencilTest() -- i guess this resets/removes stencil

      self.msg:clear()
      local text = ""
      local foo = string.sub(self.weaponOfChoice, 1, 3)
      if self.weaponOfChoice == "" then
        text = "0"
      elseif foo == "RFC" then
        text = string.sub(self.weaponOfChoice, 4)
      else
        text = inventory.Saldos[w_id]
      end
      love.graphics.setColor(0, 0, 0)
      love.graphics.circle("fill", self.x, self.y, 30)
      local index = self.msg:addf(text, 30, "center", 0, 0, 0, 2, 2)
      love.graphics.setColor(0, 255, 0)
      love.graphics.draw(self.msg, self.x-33, self.y-18)

  end
end

function Weaponconsole:update(dt)
  if not(self.initiated) then
    self.initiated = true
    stencil = function () love.graphics.circle("fill", self.x+50, self.y+50, 80) end
  end

  -- fix: zero out the animNow flag when the tween is complete
  local c = tv("WCTextX")
  if c > 99 and self.animNow then self.animNow = false end
end

function Weaponconsole:switchWeapon(isStartGame)
  -- when user presses button 2 (P), weapon switches to next
  local currentWeapon = self.weaponOfChoice
  local w_id = inventory:returnIndex(self.weaponOfChoice)
  self.previousWeapon = currentWeapon
  self.weaponOfChoice = inventory:getNextWeapon(w_id, isStartGame)
  self:initiateWeapon(self.weaponOfChoice) -- this does nothing atm

  w_id = inventory:returnIndex(self.weaponOfChoice)

  local txt
  if not(self.weaponOfChoice == "") then txt = "Selected " .. inventory.Items[w_id].name .. "." else txt = "Inventory empty." end
  textLogger:newMessage(txt, "green")
  tweenEngine:createTween("WCTextX", 0, 100, 0.2, linearTween)
  tweenEngine:createTween("WCAlpha", 255, 0, 0.2, linearTween)

  self.animNow = true
end

function Weaponconsole:initiateWeapon(w_id)
  self.weaponOfChoice = w_id
end

function Weaponconsole:fireWeapon(x, y, angle)
  local w_id = inventory:returnIndex(self.weaponOfChoice)

  if self.weaponOfChoice == "RFCI" then
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))

    if not(isAudioEffectsOff) then
      -- shooting soundfx
      local soundId = math.random(4) + 6 -- random soundId between 7 and 10
      audio.SoundFx[soundId]:stop()
      audio.SoundFx[soundId]:setVolume(0.1)
      audio.SoundFx[soundId]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
      audio.SoundFx[soundId]:play()
    end

  elseif self.weaponOfChoice == "RFCII" then
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*80),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*80),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*80),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*80),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))

    if not(isAudioEffectsOff) then
      -- shooting soundfx
      local soundId = math.random(4) + 6 -- random soundId between 7 and 10
      audio.SoundFx[soundId]:stop()
      audio.SoundFx[soundId]:setVolume(0.1)
      audio.SoundFx[soundId]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
      audio.SoundFx[soundId]:play()
    end

  elseif self.weaponOfChoice == "RFCIII" then
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*20),
      maxrange = 100000,
      dir = angle, -- testing where de fuck are my bullets
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*80),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*80),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*80),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*80),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle))+(math.cos(angle-math.pi/2)*100),
      y = y+(math.sin(angle))+(math.sin(angle-math.pi/2)*100),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle))+(math.cos(angle+math.pi/2)*100),
      y = y+(math.sin(angle))+(math.sin(angle+math.pi/2)*100),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))

    if not(isAudioEffectsOff) then
      -- shooting soundfx
      local soundId = math.random(4) + 6 -- random soundId between 7 and 10
      audio.SoundFx[soundId]:stop()
      audio.SoundFx[soundId]:setVolume(0.1)
      audio.SoundFx[soundId]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
      audio.SoundFx[soundId]:play()
    end

  elseif self.weaponOfChoice == "RFCIV" then
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle-math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle-math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*40)+(math.cos(angle+math.pi/2)*20),
      y = y+(math.sin(angle)*40)+(math.sin(angle+math.pi/2)*20),
      maxrange = 100000,
      dir = angle,
      type = 1,
      speed = 4000}))

    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*60)+(math.cos(angle-math.pi/2)*60),
      y = y+(math.sin(angle)*60)+(math.sin(angle-math.pi/2)*60),
      maxrange = 100000,
      dir = angle-math.pi/16,
      type = 1,
      speed = 4000}))
    table.insert(bullets, Bullet:new({
      x = x+(math.cos(angle)*60)+(math.cos(angle+math.pi/2)*60),
      y = y+(math.sin(angle)*60)+(math.sin(angle+math.pi/2)*60),
      maxrange = 100000,
      dir = angle+math.pi/16,
      type = 1,
      speed = 4000}))

        -- note RFCIV also shoots mine projectiles with separate fire function

    if not(isAudioEffectsOff) then
      -- shooting soundfx
      local soundId = math.random(4) + 6 -- random soundId between 7 and 10
      audio.SoundFx[soundId]:stop()
      audio.SoundFx[soundId]:setVolume(0.1)
      audio.SoundFx[soundId]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
      audio.SoundFx[soundId]:play()
    end

  elseif self.weaponOfChoice == "DIM" then -- is a mine!
    table.insert(mines, Mine:new({
      xspeed = ship.xspeed + math.cos(angle)*20,
      yspeed = ship.yspeed + math.sin(angle)*20,
      type = "normal",
      angle = angle,
      targetAngle = angle,
      x = x,
      y = y}))
      inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1

  elseif self.weaponOfChoice == "ST" or self.weaponOfChoice == "RFT" or self.weaponOfChoice == "RGT" or self.weaponOfChoice == "RLT" then
    generateTurret(x, y, self.weaponOfChoice)

  elseif self.weaponOfChoice == "DIR" then -- is a rocket!
    table.insert(rockets, Rocket:new({
      dir = angle,
      speed = 1000,
      type = "large",
      x = x,
      y = y}))
      inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1
    if not(isAudioEffectsOff) then
      audio.SoundFx[27]:stop()
      audio.SoundFx[27]:setVolume(0.1)
      audio.SoundFx[27]:setPitch(0.95 + math.random()/10) -- randomize pitch a little
      audio.SoundFx[27]:play()
    end

  elseif self.weaponOfChoice == "HW" then -- is a heavy wall!
    generateAsteroid(x+(math.cos(angle)*100), y+(math.sin(angle)*100), "heavy")
    inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1

  elseif self.weaponOfChoice == "LW" then -- is a heavy wall!
    generateAsteroid(x+(math.cos(angle)*100), y+(math.sin(angle)*100), "light")
    inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1
  end

  if inventory.Saldos[w_id] == 0 then self:switchWeapon() end
  ship.heat = ship.heatp
end

function Weaponconsole:fireMineProjectiles(x, y, angle)
  if self.weaponOfChoice == "RFCIV" then
    table.insert(mines, Mine:new({ -- RFCIV shoots also projectile-type mines
      x = x+(math.cos(angle))+(math.cos(angle+math.pi/2)*160),
      y = y+(math.sin(angle))+(math.sin(angle+math.pi/2)*160),
      xspeed = math.cos(angle)*40,
      yspeed = math.sin(angle)*40,
      angle = angle,
      targetAngle = angle - math.pi/4,
      type = "projectile"}))
    table.insert(mines, Mine:new({ -- RFCIV shoots also projectile-type mines
      x = x+(math.cos(angle))+(math.cos(angle-math.pi/2)*160),
      y = y+(math.sin(angle))+(math.sin(angle-math.pi/2)*160),
      xspeed = math.cos(angle)*40,
      yspeed = math.sin(angle)*40,
      angle = angle,
      targetAngle = angle + math.pi/4,
      type = "projectile"}))

    ship.mheat = ship.mheatp
  end
end
