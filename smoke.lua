Smoke = {}

function Smoke:new(params)
  o = {}
  o.initiated = false
  o.removeMe = false
  o.SmokeState = 0
  o.SmokeSprite = 1
  o.x = params.x
  o.y = params.y
  o.angle = params.angle
  o.size = params.size*4

  setmetatable(o, self)
  self.__index = self
  return o
end

function Smoke:draw()
  if self.initiated then
--    love.graphics.setColor(255, 255, 255, 255-self.SmokeState)
--    love.graphics.draw(smokeSprites[self.SmokeSprite], self.x, self.y, self.angle, self.size/10, self.size/10, 30, 30)
    love.graphics.setColor(200, 200, 200, 255-self.SmokeState)
--    love.graphics.setPointSize(self.size)
--    love.graphics.points(self.x, self.y) -- the Smoke is 256 pixels in size
    love.graphics.rectangle("fill", self.x-self.size/2, self.y-self.size/2, self.size, self.size)
  end
end

function Smoke:update(dt)
  if not self.initiated then
    self.initiated = true
    self:initiateSmoke()
  else
    self.SmokeState = self.SmokeState + 5
    self.SmokeSprite = self.SmokeSprite + 1
    if self.SmokeSprite == 5 then self.SmokeSprite = 1 end
  end

  if self.SmokeState > 255 then
    self.removeMe = true
    return
  end

end

function Smoke:initiateSmoke()
  self.SmokeState = 1
end
