Enemyspawner = {}

function Enemyspawner:new(params)
  o = {}
  o.pathId = params.pathId
  o.velocity = 30
  o.timer = 0

  setmetatable(o, self)
  self.__index = self
  return o
end

function Enemyspawner:update(dt)
  self.timer = self.timer + 1
  if self.timer > ((100+SPAWNDELAY)-self.velocity) then
    self.timer = 0
    enemiesLeftInWave = enemiesLeftInWave - 1
    -- spawn one enemy on my path
    table.insert(enemies, Enemy:new({
      x = math.random()*universe.width,
      pathId = self.pathId,
      spread = 1500,
      speed = 400 + ySpeedIncrease}))
    if enemiesLeftInWave == 0 then
      isSpawningPaused = true
      spawningPauseStamp = love.timer.getTime()
      enemiesLeftInWave = enemyWaveSize
      enemyWaveSize = enemyWaveSize + 1000
    end
  end
end

function Enemyspawner:increaseVelocity()
  if self.velocity < 100 then self.velocity = self.velocity + 1 end
  return self.velocity
end
