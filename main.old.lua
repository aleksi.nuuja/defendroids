require 'loadimages'
require 'ship'
require 'bullet'
require 'enemy'
require 'turret'
require 'asteroid'
require 'gamestate'
require 'tween2'
require 'textlogger'
require 'weaponconsole'
require 'audio'
require 'railgunblasts'
require 'swarmpath'
require 'surrrge'
require 'explosion'
require 'mine'
require 'inventory'
require 'rocket'
require 'smoke'
require 'flashtext'

function love.load()
	math.randomseed(os.time())

	drawPathsDebug = false

	SHIPSIZE = 1 -- multiplier used in drawing ship and turrets, not enemies (should be obsolete!!!)
	SLOWING = 0.999 -- multiplier
	SCROLLSPEED = 1
	ZOOMDELTA = 0.9 -- factor (less than 1.0)
	SCROLLBOUNDARY = 0 -- distance from the edge when scrolling starts
	UNIVERSESIZE = 5
	CAMERAONSHIP = true -- false if enemies breach - then camera is on bottom of the screen
	MAXENEMIES = 2000 -- prevents lagging - this could be adaptive in proportion to dt


	love.mouse.setVisible(false)

	-- load all images
	loadAllImages() -- in loadimagelua

	scrolloffsetX = 0
	scrolloffsetY = 0

	universe = {
		width = bg:getWidth() * UNIVERSESIZE,
		height = bg:getHeight() * UNIVERSESIZE
	}

	ship = Ship:new({
		x = universe.width / 2,
		y = universe.height / 2,
		angle = math.pi*1.5,
	  speed = 0})

	gamestate = Gamestate:new()
	swarmPathsHandler = SwarmPath:new()
	tweenEngine = Tween:new()
	initiateTweenValues()

	textLogger = Textlogger:new({
		maxrows = 3,
	  rowheight = 50,
		textsize = 24,
	  updateSpeed = 3, -- seconds (how often log scrolls on it's own)
	  x = 50,
	  y = love.graphics.getHeight() - 100 - 3*70,
		blinkDuration = 0.050, -- milliseconds how quickly new message blinks
	  maxBlinks = 3})

	weaponConsole = Weaponconsole:new({
		x = love.graphics.getWidth()/2 - 70,
	  y = love.graphics.getHeight() - 100})

	audio = Audio:new({})
	inventory = Inventory:new({})

	bullets = {}
	enemies = {}
	turrets = {}
	asteroids = {}
	railGunBlasts = {}
	surrrges = {}
	explosions = {}
	smokes = {}
	mines = {}
	rockets = {}
	flashtexts = {}

	radarOn = true
	drawnEnemyCoords = {}

	currentFPS = love.timer.getFPS()
	lowestFPS = 30
	highestFPS = 30
end

function initiateTweenValues()
	tweenEngine:newKeyAndValue("SCALE", 1)
	tweenEngine:newKeyAndValue("TEXTSIZE", 5)
	tweenEngine:newKeyAndValue("TEXTY", love.graphics.getHeight()-80)
	tweenEngine:newKeyAndValue("ALPHA", 255)
	tweenEngine:newKeyAndValue("LOGTEXTY", 0)
	tweenEngine:newKeyAndValue("LOGALPHA", 0)
	tweenEngine:newKeyAndValue("WCTEXTX", 0)
	tweenEngine:newKeyAndValue("WCALPHA", 0)
	tweenEngine:newKeyAndValue("MUSICVOLUME", 0)
	tweenEngine:newKeyAndValue("CAMERAY", 0)
	tweenEngine:newKeyAndValue("CREDITSMETER", 0)
end


function love.draw()

		if not (gamestate.state == "mainmenu") and not (gamestate.state == "shop") then
			-- first draw zoomable game graphics
			love.graphics.push()
			love.graphics.setColor(255, 255, 255)
			love.graphics.setLineWidth(1)
			love.graphics.scale(tv("SCALE"), tv("SCALE"))
			love.graphics.translate(scrolloffsetX, scrolloffsetY)

			-- draw background image which is as large as the game universe
	    love.graphics.draw(bg, 0, 0, 0, UNIVERSESIZE, UNIVERSESIZE)

			-- draw parallax scrolling second layer backround
	--    love.graphics.draw(bg2, scrolloffsetX/10, scrolloffsetY/10, 0, UNIVERSESIZE*2, UNIVERSESIZE*2)

			drawBullets()
			drawEnemies()
			drawTurrets()
			drawAsteroids()
			drawRailGunBlasts()
			drawMines()
			drawSmokes()
			drawRockets()
			ship:draw(ship.x, ship.y, ship.angle)
			drawExplosions()
			drawFlashtexts() -- draws combat-type flashtexts but ignores shop-type flashtexts

			swarmPathsHandler:draw()

			-- then reset transformations and draw static overlay graphics such as texts and menus
			love.graphics.pop()
			textLogger:draw()
			weaponConsole:draw()
			if radarOn then drawRadar() end
		end

		gamestate:draw()
		drawFlashtexts() -- draws shop-type flashtexts but ignores combat-type flashtexts

		love.graphics.print("Current FPS: " .. tostring(currentFPS), 10, 10)
--		love.graphics.print("SCALE: " .. SCALE, 10, 50)
end

function drawMines()
		for i, o in ipairs(mines) do
			o:draw(o.x, o.y, o.angle)
		end
end

function drawExplosions()
		for i, o in ipairs(explosions) do
			o:draw()
		end
end

function drawFlashtexts()
		for i, o in ipairs(flashtexts) do
			o:draw()
		end
end

function drawSmokes()
		for i, o in ipairs(smokes) do
			o:draw()
		end
end

function drawRailGunBlasts()
		for i, o in ipairs(railGunBlasts) do
			o:draw()
		end
end

function drawBullets()
		for i, o in ipairs(bullets) do
			o:draw()
		end
end

function drawRockets()
		for i, o in ipairs(rockets) do
			o:draw(o.x, o.y, o.dir, 0.2)
		end
end

function drawEnemies()
	drawnEnemyCoords = {} -- list all drawn coordinates and do not draw duplicates
	local counter = 0
	for i, o in ipairs(enemies) do
		if not(isCoordinateOnList(o.x, o.y)) then
			table.insert(drawnEnemyCoords, {o.x, o.y})
			o:draw(o.x, o.y, o.angle, 1)
		end
	end
end

function isCoordinateOnList(x, y)
	local testCoords = {x, y}
	local threshold = 10
	for i=1,#drawnEnemyCoords do
		if math.abs(drawnEnemyCoords[i][1] - testCoords[1]) < threshold and math.abs(drawnEnemyCoords[i][2] - testCoords[2]) < threshold then return true end
	end
	return false
end


function drawTurrets()
		for i, o in ipairs(turrets) do
			o:draw(o.x, o.y, o.angle, 1)
		end
end

function drawAsteroids()
		for i, o in ipairs(asteroids) do
			o:draw(o.x, o.y)
		end
end

function drawRadar() -- is a black box where enemies are plotted
	love.graphics.setColor(0, 0, 0)
	love.graphics.rectangle("fill", love.graphics.getWidth()-50, 20, 40, love.graphics.getHeight()-40)

	local radarX
	local radarY

	love.graphics.setColor(255, 0, 150)
	love.graphics.setPointSize(3)
	for i, o in ipairs(enemies) do
		radarX = o.x/universe.width*40 + love.graphics.getWidth()-50
		radarY = o.y/(universe.height) * (love.graphics.getHeight()-40) + 20
		if o.alive then love.graphics.points(radarX, radarY) end
	end
	love.graphics.setColor(255, 255, 255)
	love.graphics.setPointSize(5)
	for i, o in ipairs(turrets) do
		radarX = o.x/universe.width*40 + love.graphics.getWidth()-50
		radarY = o.y/(universe.height) * (love.graphics.getHeight()-40) + 20
		love.graphics.points(radarX, radarY)
	end
	love.graphics.setColor(0, 255, 0)
	radarX = ship.x/universe.width*40 + love.graphics.getWidth()-50
	radarY = ship.y/(universe.height) * (love.graphics.getHeight()-40) + 20
	love.graphics.points(radarX, radarY)
end

function love.keypressed(key)
		if key == "escape" then
			print("Lowest FPS: " .. lowestFPS)
			print("Highest FPS: " .. highestFPS)
  	  love.event.quit()
		end
		if key == "s" and gamestate.state == "mainmenu" then
			mainMenu:next()
		end
		if key == "w" and gamestate.state == "mainmenu" then
			mainMenu:previous()
		end

		if gamestate.state == "shop" then

			if key == "a" then
				shop:previousShopItem()
			elseif key == "d" then
				shop:nextShopItem()

			elseif key == "o"  then
				shop:buyItem(shop.shopItem)
			elseif key == "p" then
				shop:sellItem(shop.shopItem)
			end

		end

		if (key == "o" or key == "p" or key == "space") and gamestate.state == "mainmenu" then
			gamestate.substate = gamestate.substate + 1
		end
		if key == "space" and (gamestate.state == "alert" or gamestate.state == "nextlevel" or gamestate.state == "defeat" or gamestate.state == "shop") then
  	  gamestate.substate = gamestate.substate + 1
		end
		if key == "z" then -- use for debugging calls
			if drawPathsDebug then drawPathsDebug = false else drawPathsDebug = true end
		end
		if key == "p" and (gamestate.state == "combat" or gamestate.state == "startgame") and not weaponConsole.animNow then
			weaponConsole:switchWeapon()
		end
		if key == "space" and gamestate.state == "startgame" then
			if gamestate.timeRemain > 3 then -- pressing space skips waiting time to 3 seconds
				gamestate.stateDuration = gamestate.stateDuration - (gamestate.timeRemain-3)
			end
		end
		if key == "o" and gamestate.state == "startgame" then
			weaponConsole:fireWeapon()
		end
		if key == "r" then -- toggle radar
			if radarOn then radarOn = false else radarOn = true end
		end
		if key == "2" then
			local currentScale = tv("SCALE")
			tweenEngine:createTween("SCALE", currentScale, love.graphics.getHeight() / universe.height, 0.5, easeOutQuint)
		end
		if key == "1" then
			local currentScale = tv("SCALE")
			tweenEngine:createTween("SCALE", currentScale, love.graphics.getWidth() / universe.width, 0.5, easeOutQuint)
		end
end

function love.update(dt)
		-- track FPS
		currentFPS = love.timer.getFPS()
		if currentFPS < lowestFPS then lowestFPS = currentFPS end
		if currentFPS > highestFPS then highestFPS = currentFPS end

		if audio.fadingInMusic then
			audio.Music[1]:setVolume(tv("MUSICVOLUME"))
			audio.Music[2]:setVolume(tv("MUSICVOLUME"))
		end
		if MUSICVOLUME == 0.8 then audio.fadingInMusic = false end

		if CAMERAONSHIP then calculateOffsets(ship.x, ship.y) else calculateOffsets(universe.width/2, tv("CAMERAY")) end

		if not (gamestate.state == "alert") and not (gamestate.state == "nextlevel") and not (gamestate.state == "defeat") then
			-- key controls
	    if love.keyboard.isDown("a") then
	    	if love.keyboard.isDown("o") then
	    		ship.angle = ship.angle - (ship.turnspeed*dt/5)
				else ship.angle = ship.angle - ship.turnspeed*dt
				end
				if ship.angle < 0 then ship.angle = ship.angle + (2*math.pi) end
	    end

	    if love.keyboard.isDown("d") then
	    	if love.keyboard.isDown("o") then
	    		ship.angle = ship.angle + (ship.turnspeed*dt/5)
				else ship.angle = ship.angle + ship.turnspeed*dt
				end
				if ship.angle > (2*math.pi) then ship.angle = ship.angle - (2*math.pi) end
	  	end

	    if love.keyboard.isDown("w") then
					ship.xspeed = ship.xspeed + math.cos(ship.angle)*dt*60
					ship.yspeed = ship.yspeed + math.sin(ship.angle)*dt*60
	    end

			if love.keyboard.isDown("s") then
					ship.xspeed = ship.xspeed - math.cos(ship.angle)*dt*60
					ship.yspeed = ship.yspeed - math.sin(ship.angle)*dt*60
	    end

			-- bullet code copypaste:
			ship.heat = math.max(0, ship.heat - dt) -- cannon heat decreases - can shoot when heat zero
			ship.mheat = math.max(0, ship.mheat - dt) -- mine projectile cannon heat decreases - can shoot when heat zero

			if love.keyboard.isDown("o")  and ship.heat <= 0 and (gamestate.state == "combat" or gamestate.state == "victory") then
				weaponConsole:fireWeapon()
			end
			if love.keyboard.isDown("o")  and ship.mheat <= 0 and (gamestate.state == "combat" or gamestate.state == "victory") then
				weaponConsole:fireMineProjectiles()
			end

		end

		ship:update(dt)
		gamestate:update(dt)
		tweenEngine:update(dt)
		textLogger:update(dt)
		audio:update(dt)
		inventory:update(dt)
		weaponConsole:update(dt) -- when weaponConsole initiates, inventory must have already initiated!
		swarmPathsHandler:update(dt)

		-- update bullets:
		local i2, o
		for i2, o in ipairs(bullets) do
			o:update(dt)
			if o.removeMe then table.remove(bullets, i2) end
		end

		-- update rockets:
		local i2, o
		for i2, o in ipairs(rockets) do
			o:update(dt)
			if o.removeMe then table.remove(rockets, i2) end
		end

		-- update mines:
		local i2, o
		for i2, o in ipairs(mines) do
			o:update(dt)
			if o.removeMe then table.remove(mines, i2) end
		end

		-- update explosions:
		local i2, o
		for i2, o in ipairs(explosions) do
			o:update(dt)
			if o.removeMe then table.remove(explosions, i2) end
		end

		-- update flashtexts:
		local i2, o
		for i2, o in ipairs(flashtexts) do
			o:update(dt)
			if o.removeMe then table.remove(flashtexts, i2) end
		end

		-- update smokes = smoke puffs:
		local i2, o
		for i2, o in ipairs(smokes) do
			o:update(dt)
			if o.removeMe then table.remove(smokes, i2) end
		end

		-- update enemy surges:
		local i2, o
		for i2, o in ipairs(surrrges) do
			o:update(dt)
			if o.removeMe then table.remove(surrrges, i2) end
		end

		-- update railgun blasts:
		local i2, o
		for i2, o in ipairs(railGunBlasts) do
			o:update(dt)
			if railGunBlasts[i2].removeMe then table.remove(railGunBlasts, i2) end
		end

		-- update enemies:
		local i2, o
		for i2, o in ipairs(enemies) do
			o:update(dt)
			if enemies[i2].explosionState >= 13 then removeEnemy(i2) end
		end

		-- update turrets:
		local i2, o
		for i2, o in ipairs(turrets) do
			o:update(dt)
		end

		-- update asteroids:
		local i2, o
		for i2, o in ipairs(asteroids) do
			o:update(dt)
		end

		-- Check collisions:
		-- bullets hitting enemies
		for i, enemy in ipairs(enemies) do
			for j, bullet in ipairs(bullets) do
				if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, bullet.x, bullet.y, 10, 10) and enemies[i].alive then
					table.remove(bullets, j)
					enemies[i].alive = false
				end
			end

			-- enemies hitting turrets (remove both, add explosion)
			for j, turret in ipairs(turrets) do
				if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, turret.x-20, turret.y-20, 40, 40) and enemies[i].alive then
					table.remove(turrets, j)
					enemies[i].alive = false
					textLogger:newMessage("You lost a turret!", "red")
					createFlashtext(turret.x, turret.y, "YOU LOST A TURRET", "red", "combat")
					-- explosion!
					table.insert(explosions, Explosion:new({
						x = turret.x,
						y = turret.y,
						size = 4,
						angle = math.random()*math.pi*2}))
				end
			end

			-- enemies hitting mines
			for j, mine in ipairs(mines) do
				if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, mine.x-20, mine.y-20, 40, 40) and enemies[i].alive then
					enemies[i].alive = false
					table.remove(mines, j)
					-- explosion!
					local explosize = 8
					if mine.type == "projectile" then explosize = 2 end
					table.insert(explosions, Explosion:new({
						x = mine.x,
						y = mine.y,
						size = explosize,
						angle = math.random()*math.pi*2}))
				end
			end

			-- enemies hitting rockets
			for j, rocket in ipairs(rockets) do
				if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, rocket.x-20, rocket.y-20, 40, 40) and enemies[i].alive then
					enemies[i].alive = false
					table.remove(rockets, j)
					-- explosion!
					table.insert(explosions, Explosion:new({
						x = rocket.x,
						y = rocket.y,
						size = 3,
						angle = math.random()*math.pi*2}))
				end
			end


			-- enemies hitting explosion bounding boxes (being destroyed by blasts)
			for j, exp in ipairs(explosions) do
				if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, exp.boundingBoxX, exp.boundingBoxY, exp.boundingBoxWidth, exp.boundingBoxHeight) and enemies[i].alive then
					enemies[i].alive = false
					exp.killcounter = exp.killcounter + 1
					-- increase counter how many enemies this explosion destroyed - display this in a flashtext "524 kills"
				end
			end

			-- enemies hitting player
			if CheckCollision(enemy.x-40, enemy.y-40, 80, 80, ship.x-20, ship.y-20, 40, 40) and enemies[i].alive and ship.alive and not(ship.indestructable) then
				enemies[i].alive = false
				if not(ship.hurt) then -- first collision gets you hurt and you start emitting smoke
					ship.hurt = true
					ship.indestructable = true
				else
					ship.alive = false
					gamestate.breach = true -- this is game over
				end
			end

			-- enemies hitting asteroids
			for j, asteroid in ipairs(asteroids) do
				if CheckCollision(enemy.x, enemy.y, 40, 40, asteroid.x, asteroid.y, asteroid.width, asteroid.height) and enemies[i].alive then
					asteroid.xspeed = asteroid.xspeed + enemy.xspeed / 100000
					asteroid.yspeed = asteroid.yspeed + enemy.yspeed / 100000
 					if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 1 end
					if asteroid.hitpoints <= 0 then table.remove(asteroids, j) end
					enemies[i].alive = false
					if asteroid.type == "heavy" then
						-- find out on which path segment the collision occurs and increase the corresponding counter
						-- this is the index of the coordinate on the Paths[] list, which is a list of x,y coordinates, dividing it by 2 gets the segment number
						local collidingSegmentStartY = swarmPathsHandler:getPreviousPoint(enemy.y, enemy.pathId)
						local collidingSegment = collidingSegmentStartY/2

						swarmPathsHandler.collisionCounter[enemies[i].pathId][collidingSegment] = swarmPathsHandler.collisionCounter[enemies[i].pathId][collidingSegment] + 1
						swarmPathsHandler.collisionY[enemies[i].pathId] = enemy.y -- set the collisionY on this segment to the Y of last collision occurred
					end
				end
			end

			-- enemies hitting bottom wall -- this is a breach and means game over!
			if enemy.y > universe.height and enemy.alive then
				gamestate.breach = true
				if CAMERAONSHIP then tweenEngine:createTween("CAMERAY", ship.y, universe.height, 2, easeInOutCubic) end
				CAMERAONSHIP = false -- moves camera location to bottom of universe
				table.remove(enemies, i)
			end
		end -- end of enemy collisions for loop

		-- asteroids hitting turrets (when pushed on top)
		for i, asteroid in ipairs(asteroids) do
			for j, turret in ipairs(turrets) do
				if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, turret.x-40, turret.y-40, 80, 80) then
					table.remove(turrets, j)
					textLogger:newMessage("You lost a turret!", "red")
					createFlashtext(turret.x, turret.y, "YOU LOST A TURRET", "red", "combat")
					-- explosion!
					table.insert(explosions, Explosion:new({
						x = turret.x,
						y = turret.y,
						size = 4,
						angle = math.random()*math.pi*2}))
				end
			end
		end

		-- bullets hitting asteroids
		for i, asteroid in ipairs(asteroids) do
			for j, bullet in ipairs(bullets) do
				if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, bullet.x-5, bullet.y-5, 10, 10) then
					table.remove(bullets, j)
					asteroid.xspeed = asteroid.xspeed + bullet.xspeed / 100000
					asteroid.yspeed = asteroid.yspeed + bullet.yspeed / 100000
					if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 1 end
					if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
				end
			end
		end

		-- bullets hitting "normal" type mines - they move a little
		for i, mine in ipairs(mines) do
			if mine.type == "normal" then
				for j, bullet in ipairs(bullets) do
					if CheckCollision(mine.x-50, mine.y-50, 100, 100, bullet.x-5, bullet.y-5, 10, 10) then
						table.remove(bullets, j)
						mine.xspeed = mine.xspeed + bullet.xspeed / 25000
						mine.yspeed = mine.yspeed + bullet.yspeed / 25000
					end
				end
			end
		end

		-- rockets hitting asteroids
		for i, asteroid in ipairs(asteroids) do
			for j, rocket in ipairs(rockets) do
				if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, rocket.x-5, rocket.y-5, 10, 10) then
					table.remove(rockets, j)
					asteroid.xspeed = asteroid.xspeed + rocket.xspeed / 5000
					asteroid.yspeed = asteroid.yspeed + rocket.yspeed / 5000
					if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 10 end
					if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
					-- explosion!
					table.insert(explosions, Explosion:new({
						x = rocket.x,
						y = rocket.y,
						size = 3,
						angle = math.random()*math.pi*2}))
				end
			end
		end

		-- mines hitting asteroids
		for i, asteroid in ipairs(asteroids) do
			for j, mine in ipairs(mines) do
				if CheckCollision(asteroid.x, asteroid.y, asteroid.width, asteroid.height, mine.x-50, mine.y-50, 100, 100) then
					table.remove(mines, j)
					asteroid.xspeed = asteroid.xspeed + mine.xspeed / 500
					asteroid.yspeed = asteroid.yspeed + mine.yspeed / 500
					if asteroid.type == "light" then asteroid.hitpoints = asteroid.hitpoints - 10 end
					if asteroid.hitpoints <= 0 then table.remove(asteroids, i) end
					-- explosion!
					local explosize = 8
					if mine.type == "projectile" then explosize = 2 end
					table.insert(explosions, Explosion:new({
						x = mine.x,
						y = mine.y,
						size = explosize,
						angle = math.random()*math.pi*2}))
				end
			end
		end


		-- bullets hitting turrets -- disabled MAYBE IT'S MORE FUN TO LET TURRETS CROSSFIRE
--		for i, turret in ipairs(turrets) do
--			for j, bullet in ipairs(bullets) do
--				if CheckCollision(turret.x-20, turret.y-20, 40, 40, bullet.x, bullet.y, 10, 10) then
--					table.remove(turrets, i)
--					table.remove(bullets, j)
--					textLogger:newMessage("You lost a turret!", "red")
--				end
--			end
--		end

		-- player hitting asteroids
		local deadmanswitch = 0
		for i = 1, #asteroids do
			if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ship.x-70, ship.y-70, 140, 140) then
				local slowdown
--				if asteroids[i].type == "heavy" then slowdown = 0.6 else slowdown = 0.8 end
				asteroids[i].xspeed = ship.xspeed --* slowdown
				asteroids[i].yspeed = ship.yspeed --* slowdown
				ship.xspeed = ship.xspeed --* slowdown
				ship.yspeed = ship.yspeed --* slowdown
				repeat
					deadmanswitch = deadmanswitch + 1
					asteroids[i].x = asteroids[i].x + ship.xspeed
					asteroids[i].y = asteroids[i].y + ship.yspeed
				until deadmanswitch > 1000 or not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ship.x-70, ship.y-70, 140, 140))
				if deadmanswitch > 1000 then print("player hitting asteroid repeat loop just used dead man's switch of over 1000 repeats") end
			end
		end

		-- asteroids hitting asteroids
		local count = 1
		for i = 1, #asteroids-1 do
			for j = 1 + count, #asteroids do
					if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height) then

						-- calculate speed for both asteroids, adapt the faster moving asteroid's speed for both asteroids
						local iSpeed = math.sqrt(asteroids[i].xspeed * asteroids[i].xspeed + asteroids[i].yspeed * asteroids[i].yspeed)
						local jSpeed = math.sqrt(asteroids[j].xspeed * asteroids[j].xspeed + asteroids[j].yspeed * asteroids[j].yspeed)

						if iSpeed > jSpeed then
							asteroids[j].xspeed = asteroids[i].xspeed
							asteroids[j].yspeed = asteroids[i].yspeed
						else
							asteroids[i].xspeed = asteroids[j].xspeed
							asteroids[i].yspeed = asteroids[j].yspeed
						end

						-- try and move the first one a bit and test if they still overlap, if yes, cancel the move and move the other one bit and test if they overlap
						-- if still yes, increase the step and try again until you find a step that frees the willies!!!
						local clearedOverlap = false
						local step = 1
						repeat
							asteroids[i].x = asteroids[i].x + asteroids[i].xspeed*step
							asteroids[i].y = asteroids[i].y + asteroids[i].yspeed*step
							if not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height)) then clearedOverlap = true end
							if not clearedOverlap then
								asteroids[i].x = asteroids[i].x - asteroids[i].xspeed*step
								asteroids[i].y = asteroids[i].y - asteroids[i].yspeed*step
								asteroids[j].x = asteroids[j].x + asteroids[j].xspeed*step
								asteroids[j].y = asteroids[j].y + asteroids[j].yspeed*step
								if not(CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, asteroids[j].x, asteroids[j].y, asteroids[j].width, asteroids[j].height)) then clearedOverlap = true end
								if not clearedOverlap then
									asteroids[j].x = asteroids[j].x - asteroids[j].xspeed*step
									asteroids[j].y = asteroids[j].y - asteroids[j].yspeed*step
								end
							end
							step = step + 1

						until clearedOverlap or step > 10

					end
			end
			count = count + 1 -- for each round there are less to check - to find all possible pairs
		end

end

function removeEnemy(i)
	gamestate.kills = gamestate.kills + 1
	gamestate.credits = gamestate.credits + 5
	table.remove(enemies, i)

	-- explosion soundfx
	if audio.expDelay < 0 then
		audio.expRot = audio.expRot + 1
		if audio.expRot == 4 then audio.expRot = 1 end
		audio.SoundFx[audio.expRot]:stop()
		audio.SoundFx[audio.expRot]:setVolume(0.2 + math.random(3)/10)
		audio.SoundFx[audio.expRot]:setPitch(0.70 + math.random()/40*2) -- one octave lower
		audio.SoundFx[audio.expRot]:play()
		audio.expDelay = math.random()*0.5 + 0.1
	end
end


-- center camera to x, y by calculating correct scrolloffset
-- except when close to universe boundaries
function calculateOffsets(x, y)

	local screenwidth = (love.graphics.getWidth() / tv("SCALE"))
	local screenheight = (love.graphics.getHeight() / tv("SCALE"))
	local midpointx = - scrolloffsetX + (screenwidth  / 2)
	local midpointy = - scrolloffsetY + (screenheight / 2)

	-- so the delta to move scrolloffset is the difference between where the ship is drawn and the midpoint
	local deltax = midpointx - x
	local deltay = midpointy - y

	-- calculate distance from universe edge
	local xdistance = universe.width - midpointx
	local ydistance = universe.height - midpointy

	-- determine if ship coordinates are near boundary or in the middle - this affects scrolling
	-- values as strings "start", "mid", "end"
	local xarea = "mid"
	local yarea = "mid"
	if x < (screenwidth / 2) then xarea = "start" end
	if x > (universe.width - (screenwidth / 2)) then xarea = "end" end
	if y < (screenheight / 2) then yarea = "start" end
	if y > (universe.height - (screenheight / 2)) then yarea = "end" end

	-- in mid area, scroll freely
	if xarea == "mid" then scrolloffsetX = scrolloffsetX + deltax end
	if yarea == "mid" then scrolloffsetY = scrolloffsetY + deltay end

	-- if close to zero, that is "start", do nothing, offset remains put

	-- if close to end of universe boundary, that is "end" calculate correct offset
	-- it's universe boundary - screenwidth/height
	if xarea == "end" then scrolloffsetX = - (universe.width - screenwidth) end
	if yarea == "end" then scrolloffsetY = - (universe.height - screenheight) end

  if not(tv("SCALE") >= (love.graphics.getWidth() / universe.width)) then
		-- if scale is so zoomed out that the universe width is smaller in width than the screen, center it
		local actualWidthOfUniverse = universe.width*tv("SCALE")
		local centerXDelta = (love.graphics.getWidth() - actualWidthOfUniverse)/2
		scrolloffsetX = centerXDelta/tv("SCALE")
	end

end

-- params.eter turret is a string "ST", "RFT" ... etc.
function generateTurret(x, y, turret)
	local w_id = inventory:returnIndex(turret)
	local room = true
	local i
	for i=1, #turrets do
		if CheckCollision(ship.x-20, ship.y-20, 40, 40, turrets[i].x-80, turrets[i].y-80, 160, 160) then room = false end
	end
	if room then
		inventory.Saldos[w_id] = inventory.Saldos[w_id] - 1 -- index of turrets in inventory is hard coded
		table.insert(turrets, Turret:new({
			x = x,
			y = y,
			angle = math.pi/2,
			type = turret})) -- type 2 spreadfire superturrets, type 3 autoaim railgun, type 4 rocket launcher
	else
		textLogger:newMessage("Too close to existing turret!", "green")
	end
end

function generateAsteroid(x, y, type)
	table.insert(asteroids, Asteroid:new({
		type = type,
		x = x,
		y = y}))
end

function createFlashtext(x, y, msg, col, type)

	if gamestate.state == "shop" then -- in shop, only one flashtext at a time, when new created clear all others
		flashtexts = {}
	end

	table.insert(flashtexts, Flashtext:new({
		duration = 100,
		colour = col,
		type = type,
		msg = msg,
		x = x,
		y = y}))
end

function createRailGunBlast(x, y, targetx, targety)
	-- check here if the line intersects with walls
	-- we iterate along the path step by step by step and check if the point collides with the walls
	-- and if yes, then move targetx and targety to that point and return false, if the whole iteration goes through then it's fine to blast all the way

	-- calculate the angle of the line
  local angle = - math.atan2(targetx - x, targety - y) + math.pi/2
	-- calculate original length of the line
	local length = math.sqrt((targetx - x)*(targetx - x)+(targety - y)*(targety - y))

	-- iterate istep until gone over the original length of the line
	local istep = 0
	local ix = x
	local iy = y
	repeat
		istep = istep + 5
		ix = ix + math.cos(angle)*istep
		iy = iy + math.sin(angle)*istep
		-- check step by step along the line, collision with all walls (asteroids)
		for i = 1, #asteroids do
			if CheckCollision(asteroids[i].x, asteroids[i].y, asteroids[i].width, asteroids[i].height, ix-2, iy-2, 4, 4) then
				targetx = ix
				targety = iy
				table.insert(railGunBlasts, RailGunBlasts:new({
					targetX = targetx,
					targetY = targety,
					x = x,
					y = y}))
				return false
			end
		end
	until istep > length

	table.insert(railGunBlasts, RailGunBlasts:new({
		targetX = targetx,
		targetY = targety,
		x = x,
		y = y}))
	return true
end

function removeOldRailGunBlasts()
	for i, blast in ipairs(railGunBlasts) do
		if blast.removeMe then
			table.remove(railGunBlasts, i)
		end
	end
end

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function math.round(n, deci) deci = 10^(deci or 0) return math.floor(n*deci+.5)/deci end
