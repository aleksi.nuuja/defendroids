Shopbutton = {}

function Shopbutton:new(params)
  o = {}
  o.x = params.x
  o.y = params.y
  o.sprite = params.sprite

  setmetatable(o, self)
  self.__index = self
  return o
end
